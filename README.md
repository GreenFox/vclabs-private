# VCLabs

Compilation of my portfolio and codes in Objective-C and Swift. This project will keep on being updated and expanded. 

Showcase Features:
  1. "Basic UIKit Components",
  2. "Animate with Core Animation",
  3. "Image Video Filters with GPUImage",
  4. "Map & Location with Core Location",
  5. "Font Explorer with UIFont",
  6. "Device Info with UIDevice & UIScreen",
  7. "Class Explorer",
  8. "Protocol Explorer",
  9. "Unicode Explorer",
  10. "Gesture Recognizer",
  11. "Drawing with Quartz",
  12. "Color Explorer with UIColor Extensions",
  13. "Physics with UIKit Dynamics",
  14. "Device Motion with Core Motion",
  15. "Make Payment with PayPal SDK",
  16. "Dialog with Quick Dialog",
  17. "Badge for UIView",
  18. "Animate with Spring",
  19. "Camera and Photo",
  20. "Data Persistence NSUserDefaults (Offline)",
  21. "Call and Messaging",
  22. "Core Image Sample",
  23. "Core Image Suite Demo",
  
Menu Navigation: 
  1. Navigation Bar
  2. Tab Bar
  3. Side Menu

Pods Used:
  1. 'SVProgressHUD'
  2. 'SWFrameButton'
  3. 'AFNetworking'
  4. 'SDWebImage'
  5. 'BlocksKit'
  6. 'Sidecar'
  7. 'SSKeychain'
  8. 'DateTools'
  9. 'Timepiece'
  10. 'JSBadgeView'
  11. 'GBDeviceInfo'
  12. 'Spring'
  13. 'Colours/Swift'
  14. 'RESideMenu'
  15. 'Alamofire'
  16. 'SwiftyJSON'

Including Unit Testing & User Interface Testing Automation.

Disclaimer: 
These codes are made by me with additional libraries from Apple Codes and Third Party Libraries apart from mine.
