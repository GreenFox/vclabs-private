#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "CRLBlockExecution.h"
#import "CRLDigestExtensions.h"
#import "CRLFileUtils.h"
#import "CRLGeometryUtils.h"
#import "CRLGradientLayerView.h"
#import "CRLMethodLogFormatter.h"
#import "CRLShapeLayerView.h"
#import "CRLSystemSound.h"
#import "CRLViewUtils.h"
#import "NSAttributedString+CRLUtils.h"
#import "NSMutableAttributedString+CRLUtils.h"
#import "NSNumber+CRL64BitSupport.h"
#import "NSObject+CRLRuntime.h"
#import "NSString+CRLAttributedUtils.h"
#import "NSString+CRLUtils.h"
#import "NSURLComponents+CRLUtils.h"
#import "Sidecar.h"
#import "tgmath-stopgap.h"
#import "UIColor+CRLHexColors.h"
#import "UIView+CRLPositioning.h"

FOUNDATION_EXPORT double SidecarVersionNumber;
FOUNDATION_EXPORT const unsigned char SidecarVersionString[];

