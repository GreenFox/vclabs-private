//
//  UnicodeBrowser.h
//  UltimateTool
//
//  Created by Victor Chandra on 3/20/13.
//  Copyright (c) 2013 ThreeMonkee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnicodeExplorerVC : UITableViewController
<UIAlertViewDelegate>

@property (nonatomic,retain) NSArray *arrayIndex;

@end
