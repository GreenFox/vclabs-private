//
//  Syntax.swift
//  VCLabs
//
//  Created by Victor Chandra on 22/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import Foundation

let arrayOne = ["One", "Two", "Three"]
let arrayTwo: Array = ["One"]
//let arrayThree = []

