//
//  DeviceInfoVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 26/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import UIKit
import CoreMotion
import GBDeviceInfo

class DeviceInfoVC: UITableViewController
{
    let generalInfo = [
        "Name" : UIDevice.current.name,
        "Model" : UIDevice.current.model,
        "Localised Model" : UIDevice.current.localizedModel,
        "System Name" : UIDevice.current.systemName,
        "System Version" : UIDevice.current.systemVersion,
        "UUID" : UIDevice.current.identifierForVendor?.uuidString,
        "Multitasking Supported" : String(UIDevice.current.isMultitaskingSupported),
        "User Interface Idiom" : String(describing: UIDevice.current.userInterfaceIdiom),
    ]
    
    let batteryInfo = [
        "Battery Monitoring Enabled" : String(UIDevice.current.isBatteryMonitoringEnabled),
        "Battery State" : String(describing: UIDevice.current.batteryState),
        "Battery Level" : String(UIDevice.current.batteryLevel)
    ]
    
    
    var screenInfo : [String:String] = [:]
    var extraInfo : [String:String] = [:]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        title = "Device Information"
        
        let mainScreen = UIScreen.main
        self.screenInfo = [
            "Bounds" : String(describing: mainScreen.bounds),
            "Scale" : String(describing: mainScreen.scale),
            "Brightness" : String(describing: mainScreen.brightness),
            "Current Size" : String(describing: mainScreen.currentMode!.size),
            "Current Pixel Aspect Ratio" : String(describing: mainScreen.currentMode!.pixelAspectRatio),
        ]
        
        guard let deviceInfo = GBDeviceInfo.deviceInfo() else { return }
        let infoFreq = String(describing: deviceInfo.cpuInfo.frequency)
        let infoL2Cache = String(describing: deviceInfo.cpuInfo.l2CacheSize)
        let infoCores = String(describing: deviceInfo.cpuInfo.numberOfCores)
        self.extraInfo = [
            "Model" : deviceInfo.modelString,
            "Raw System Info String" : deviceInfo.rawSystemInfoString,
            "CPU Frequency" : infoFreq,
            "L2 Cache" : infoL2Cache,
            "Cores" : infoCores
        ]
        self.extraInfo["OS Version Major"] = String(describing: deviceInfo.osVersion.major)
        self.extraInfo["OS Version Minor"] = String(describing: deviceInfo.osVersion.minor)
        self.extraInfo["OS Version Patch"] = String(describing: deviceInfo.osVersion.patch)
        self.extraInfo["Device Version Major"] = String(describing: deviceInfo.deviceVersion.major)
        self.extraInfo["Device Minor Minor"] = String(describing: deviceInfo.deviceVersion.minor)
        self.extraInfo["Pixels Per Inch"] = String(describing: deviceInfo.displayInfo.pixelsPerInch)
        
        self.tableView.reloadData()
        

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section
        {
        case 0:
            return "General Information from UIDevice"
        case 1:
            return "Battery Information from UIDevice"
        case 2:
            return "Screen Information from UIScreen"
        case 3:
            return "General Information from GBDeviceInfo"
        default:
            return ""
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
        case 0:
            return generalInfo.keys.count
        case 1:
            return batteryInfo.keys.count
        case 2:
            return screenInfo.keys.count
        case 3:
            return extraInfo.keys.count
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        switch indexPath.section
        {
        case 0:
            let keys = Array(generalInfo.keys)
            cell.textLabel?.text = keys[indexPath.row]
            cell.detailTextLabel?.text = generalInfo[keys[indexPath.row]]!
        case 1:
            let keys = Array(batteryInfo.keys)
            cell.textLabel?.text = keys[indexPath.row]
            cell.detailTextLabel?.text = batteryInfo[keys[indexPath.row]]!
        case 2:
            let keys = Array(screenInfo.keys)
            cell.textLabel?.text = keys[indexPath.row]
            cell.detailTextLabel?.text = screenInfo[keys[indexPath.row]]!
        case 3:
            let keys = Array(extraInfo.keys)
            cell.textLabel?.text = keys[indexPath.row]
            cell.detailTextLabel?.text = extraInfo[keys[indexPath.row]]!
        default:
            break
        }

        return cell
    }
}
