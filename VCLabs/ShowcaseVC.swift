//
//  ShowcaseVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 22/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import UIKit

class ShowcaseVC: UITableViewController
{
    var menu = [
        
        "Loop Video Play",
        
        "Basic UIKit Components",
        "Animate with Core Animation",
        "Drawing with Quartz",
        "Image Video Filters with GPUImage",
        "Physics with UIKit Dynamics",
        "Animate with Spring",
        "Bluetooth Lab",
        "Cryptography with Hash",
        "Font Explorer with UIFont",
        "Device Info with UIDevice & UIScreen",
        "Core Image Suite Demo",
        "Class Explorer",
        "Protocol Explorer",
        "Unicode Explorer",
        "Gesture Recognizer",
        "Map & Location with Core Location",
        "Color Explorer with UIColor Extensions",
        "Device Motion with Core Motion",
        "Badge for UIView",
        "Data Persistence NSUserDefaults (Offline)",
        "Call and Messaging",
        "Core Image Sample",
        "Camera and Photo",
        "Progress HUD Demo",
        "Locale Explorer",
        
//        "BackendLess Demo",
//        "Make Payment with PayPal SDK",
//        "Plotting with Core Plot",
//        "QRCode & Barcode Reader",
//        "Data Persistence with iCloud (Online)",
//        "Data Persistence with Core Data (Offline)",
//        "Mathematical Functions",
//        "Facebook & Twitter with Social Framework"
    ]
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = menu[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch menu[indexPath.row]
        {
        case "Loop Video Play":
            let loopVideoVC = LoopVideoVC.init(nibName: "LoopVideoVC", bundle: nil)
            self.navigationController?.pushViewController(loopVideoVC, animated: true)
            
        case "Font Explorer with UIFont":
            let fontExplorer = FontExplorerVC(style: .grouped)
            self.navigationController?.pushViewController(fontExplorer, animated: true)
            
        case "Image Video Filters with GPUImage":
            let filterList = UIStoryboard.init(name: "GPUImageDemo", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(filterList!, animated: true)

        case "Device Info with UIDevice & UIScreen":
            let deviceInfo = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeviceInfoVC")
            self.navigationController?.pushViewController(deviceInfo, animated: true)
        
        case "Class Explorer":
            let classExplorer = ClassExplorerVC(style: .grouped)
            self.navigationController?.pushViewController(classExplorer, animated: true)

        case "Protocol Explorer":
            let protocolExplorer = ProtocolExplorerVC(style: .grouped)
            self.navigationController?.pushViewController(protocolExplorer, animated: true)

        case "Unicode Explorer":
            let unicodeExplorer = UnicodeExplorerVC(style: .grouped)
            self.navigationController?.pushViewController(unicodeExplorer, animated: true)

        case "Gesture Recognizer":
            let gestureRecognizer = GestureRecognizer()
            self.navigationController?.pushViewController(gestureRecognizer, animated: true)

        case "Basic UIKit Components":
            let basicUIKit = UIStoryboard(name: "UIKitCatalog", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(basicUIKit!, animated: true)
            
        case "Drawing with Quartz":
            let drawingQuartz = UIStoryboard(name: "Quartz", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(drawingQuartz!, animated: true)

        case "Physics with UIKit Dynamics":
            let uikitDynamics = UIStoryboard(name: "UIKitDynamics", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(uikitDynamics!, animated: true)
            
        case "Device Motion with Core Motion":
            let deviceMotion = UIStoryboard(name: "DeviceMotion", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(deviceMotion!, animated: true)
            
        case "Badge for UIView":
            let badgeAdd = BadgeAddVC()
            self.navigationController?.pushViewController(badgeAdd, animated: true)
            
        case "Animate with Spring":
            let animateSpring = UIStoryboard(name: "Spring", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(animateSpring!, animated: true)

        case "Map & Location with Core Location":
            let coreLocation = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CoreLocationVC")
            self.navigationController?.pushViewController(coreLocation, animated: true)

        case "Camera and Photo":
            let cameraPhoto = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraPhotoVC")
            self.navigationController?.pushViewController(cameraPhoto, animated: true)

        case "Data Persistence NSUserDefaults (Offline)":
            let userDefaults = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserDefaultsVC")
            self.navigationController?.pushViewController(userDefaults, animated: true)
            
        case "Color Explorer with UIColor Extensions":
            let colorExplorer = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ColorExplorerVC")
            self.navigationController?.pushViewController(colorExplorer, animated: true)
            
        case "Call and Messaging":
            let callMessaging = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallMessagingVC")
            self.navigationController?.pushViewController(callMessaging, animated: true)

        case "Animate with Core Animation":
            let animateCoreAnimation = UIStoryboard(name: "CoreAnimation", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(animateCoreAnimation!, animated: true)

        case "Core Image Sample":
            let sampleCoreImage = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BrightnessContrastVC")
            self.navigationController?.pushViewController(sampleCoreImage, animated: true)
            
        case "Core Image Suite Demo":
            let demoCoreImage = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterDemoVC")
            self.navigationController?.pushViewController(demoCoreImage, animated: true)
            
        case "Progress HUD Demo":
            let progressHUD = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProgressHudVC")
            self.navigationController?.pushViewController(progressHUD, animated: true)
            
        case "Cryptography with Hash":
            let hash = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CryptoHashVC")
            self.navigationController?.pushViewController(hash, animated: true)
            
//        case "BackendLess Demo":
//            let backendLess = BackendLessVC(style: .grouped)
//            self.navigationController?.pushViewController(backendLess, animated: true)

        case "Bluetooth Lab":
            let bluetoothLab = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BluetoothLabVC")
            self.navigationController?.pushViewController(bluetoothLab, animated: true)

        //case "Make Payment with PayPal SDK":
            // Put code for PayPal here
            
        case "Locale Explorer":
            let localeExplorer = LocaleExplorerVC(style: .grouped)
            self.navigationController?.pushViewController(localeExplorer, animated: true)
            
        default:
            break;
        }
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
}
