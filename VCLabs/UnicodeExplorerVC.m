//
//  UnicodeBrowser.m
//  UltimateTool
//
//  Created by Victor Chandra on 3/20/13.
//  Copyright (c) 2013 ThreeMonkee. All rights reserved.
//

#import "UnicodeExplorerVC.h"

@interface UnicodeExplorerVC ()
@property (nonatomic,strong) NSArray *arrayUnicode;
@end

@implementation UnicodeExplorerVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
        _arrayIndex = @[@"0000",@"1000",@"2000",@"3000",@"4000",@"5000",@"6000",@"7000",@"8000",@"9000",@"A000",@"B000",@"C000",@"D000",@"E000",@"F000"];
        
        NSMutableArray *tempArray = [NSMutableArray array];
        for (int i=0 ; i<65536 ; i++)
        {
//            int code = i;
//            NSString *stringCode = [NSString stringWithFormat:@"%d", code];
//            NSScanner *scanner = [NSScanner scannerWithString:stringCode];
//            [scanner scanInt:&code];
            unichar x = i;
            NSString *text = [NSString stringWithCharacters:&x length:1];
            [tempArray addObject:text];
        }
        self.arrayUnicode = tempArray;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Unicode Explorer";

    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(openSearchAlert)];
    [self.navigationItem setRightBarButtonItem:searchButton];
}


#pragma mark - Search

-(void)openSearchAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Search" message:@"Please type 4 digits Unicode Hex ex: 52dd" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeASCIICapable];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        return;
    
    NSString *stringInput = [[alertView textFieldAtIndex:0] text];
    if ([[stringInput stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        return;
    
    //NSString *stringToScan = [stringInput substringToIndex:4];
    NSString *stringToScan = stringInput;
    NSScanner *scanner = [NSScanner scannerWithString:stringToScan];
    unsigned int index = 0;
    [scanner scanHexInt:&index];
    if (index > 65535)
        index = 0;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayUnicode.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        [cell.textLabel setFont:[UIFont systemFontOfSize:42]];
        [cell setIndentationLevel:3];
    }
    //[cell.textLabel setText:self.arrayUnicode[indexPath.row]];
    [cell.textLabel setAttributedText:[[NSAttributedString alloc] initWithString:self.arrayUnicode[indexPath.row]]];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"0x %04lX", (long)indexPath.row]];
    return cell;
}

#pragma mark - Indexing

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    unsigned int foundIndex = 0;
    NSScanner *scanner = [NSScanner scannerWithString:title];
    [scanner scanHexInt:&foundIndex];
    NSLog(@"index title %d", foundIndex);
    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:foundIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    return 1;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _arrayIndex;
}

@end
