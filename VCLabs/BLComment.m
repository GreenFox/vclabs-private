//
//  BLComment.m
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import "BLComment.h"

@implementation BLComment

@synthesize authorEmail, message, objectId;

+(BLComment *)commentWithMessage:(NSString *)message authorEmail:(NSString *)authorEmail
{
    BLComment *comment = [BLComment new];
    comment.message = message;
    comment.authorEmail = authorEmail;
    return comment;
}

@end