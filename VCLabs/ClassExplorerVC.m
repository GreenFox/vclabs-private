//
//  ClassBrowser.m
//  UltimateTool
//
//  Created by Victor Chandra on 3/5/13.
//  Copyright (c) 2013 ThreeMonkee. All rights reserved.
//

#import "ClassExplorerVC.h"
#import "ClassDetailVC.h"

@interface ClassExplorerVC ()

@end

@implementation ClassExplorerVC


#pragma mark -

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Class Explorer";
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:TRUE];
        });
        self.arrayClass = [RuntimeHelper arrayClass];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:FALSE];
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_arrayClass count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"All Class (%lu)", (unsigned long)[_arrayClass count]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell.textLabel setNumberOfLines:2];
        [cell.textLabel setMinimumScaleFactor:0.5];
        [cell.textLabel setAdjustsFontSizeToFitWidth:TRUE];
    }
    
    // Configure the cell...
    cell.textLabel.text = [[_arrayClass objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    return cell;
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    return @[@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",
//    @"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z"];
//}
//
//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
//{
//    return index % 2;
//}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClassDetailVC *classDetail = [[ClassDetailVC alloc] initWithStyle:UITableViewStyleGrouped];
    NSDictionary *dictThisClass = [_arrayClass objectAtIndex:indexPath.row];
    [classDetail setDictThisClass:dictThisClass];
    [self.navigationController pushViewController:classDetail animated:TRUE];
}

@end
