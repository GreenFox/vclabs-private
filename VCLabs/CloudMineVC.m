//
//  CloudMineVC.m
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import "CloudMineVC.h"
#import <CloudMine/CloudMine.h>
#import "CMCar.h"

@interface CloudMineVC ()

@end

@implementation CloudMineVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupCloudMine];
}

- (void)setupCloudMine
{
    CMAPICredentials *credentials = [CMAPICredentials sharedInstance];
    credentials.appIdentifier = @"84e5c4a381e7424b8df62e055f0b69db";
    credentials.appSecret = @"84c8c3f1223b4710b180d181cd6fb1df";
}

- (void)createTwoCars
{
    // Create a car instance
    CMCar *porsche = [[CMCar alloc] init];
    porsche.make = @"Porsche";
    porsche.model = @"Roadster";
    porsche.year = 2012;
    
    [porsche save:^(CMObjectUploadResponse *response) {
        NSLog(@"Status: %@", [response.uploadStatuses objectForKey:porsche.objectId]);
    }];
    
    // Create another
    CMCar *honda = [[CMCar alloc] init];
    honda.make = @"Honda";
    honda.model = @"Civic";
    honda.year = 2012;
    
    [honda save:^(CMObjectUploadResponse *response) {
        NSLog(@"Status: %@", [response.uploadStatuses objectForKey:honda.objectId]);
    }];
}

@end
