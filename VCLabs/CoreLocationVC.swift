//
//  CoreLocationVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 5/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit
import CoreLocation

class CoreLocationVC: UITableViewController, CLLocationManagerDelegate
{
    let locationManager = CLLocationManager()
    var currentPlacemark:CLPlacemark?
    
    let dataSource = [
        [
            "Coordinate (Lat, Long)",
            "Altitude (meters)",
            "Placemark name",
            "Placemark ISOcountryCode",
            "Placemark country",
            "Placemark postalCode",
            "Placemark administrativeArea",
            "Placemark subAdministrativeArea",
            "Placemark locality",
            "Placemark subLocality",
            "Placemark thoroughfare",
            "Placemark subThoroughfare",
            "Placemark region",
            "Placemark timeZone",
            "Placemark inlandWater",
            "Placemark ocean",
            "Placemark areasOfInterest",
            "Current Location Information"
        ],
        [
            "locationServicesEnabled",
            "authorizationStatus",
            "deferredLocationUpdatesAvailable",
            "significantLocationChangeMonitoringAvailable",
            "headingAvailable",
            "isRangingAvailable",
            "Core Location Status"
        ],
        [
            "requestWhenInUseAuthorization",
            "requestAlwaysAuthorization",
            "Request Authorization"
        ],
        [
            "kCLLocationAccuracyBestForNavigation",
            "kCLLocationAccuracyBest",
            "kCLLocationAccuracyNearestTenMeters",
            "kCLLocationAccuracyHundredMeters",
            "kCLLocationAccuracyKilometer",
            "kCLLocationAccuracyThreeKilometers",
            "Set Desired Accuracy"
        ],
        [
            "startUpdatingLocation",
            "stopUpdatingLocation",
            "requestLocation",
            "startMonitoringSignificantLocationChanges",
            "stopMonitoringSignificantLocationChanges",
            "Get Location Updates"
        ]
    ]

    func getAuthorizationStatusString(_ status:CLAuthorizationStatus) -> String
    {
        switch status
        {
        case .notDetermined:
            return "NotDetermined"
        case .restricted:
            return "Restricted"
        case .denied:
            return "Denied"
        case .authorizedAlways:
            return "AuthorizedAlways"
        case .authorizedWhenInUse:
            return "AuthorizedWhenInUse"
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        locationManager.delegate = self
        
        //reverseGeocodeDummy()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Map", style: .plain, target: self, action: #selector(CoreLocationVC.openMap))
    }
    
    @objc func openMap ()
    {
        if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            let mapLocation = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapLocationVC")
            self.navigationController?.pushViewController(mapLocation, animated: true)
        } else
        {
            VictorKit.alertError("Not yet authorized. Please tap requestWhenInUseAuthorization or requestAlwaysAuthorization")
        }
    }
    
    func reverseGeocode ()
    {
        if locationManager.location == nil
        {
            return
        }
        
        CLGeocoder().reverseGeocodeLocation(locationManager.location!) { (placemarks, error) -> Void in
            
            if error != nil
            {
                return
            }
            
            if placemarks!.count > 0
            {
                self.currentPlacemark = placemarks![0]
            }
        }
    }
    
    func reverseGeocodeDummy ()
    {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: -37.807008, longitude: 144.958054)) { (placemarks, error) -> Void in
            
            if placemarks!.count > 0
            {
                self.currentPlacemark = placemarks![0]
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations : \(locValue.latitude) \(locValue.longitude)")
        
        reverseGeocode()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("error : \(error.localizedDescription)")
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager)
    {
        
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager)
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        self.tableView.reloadRows(at: [IndexPath(row: 1, section: 1)], with: .fade)
    }
  
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading)
    {
        
    }
    
    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool
    {
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion)
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion)
    {
        
    }

    func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error)
    {
        
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion)
    {
        
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion)
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error)
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion)
    {
        
    }

    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?)
    {
        
    }

    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit)
    {
        
    }
    
    
    // MARK: - Table view data source & delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.dataSource[section].count - 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return self.dataSource[section].last
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell
        
        switch self.dataSource[indexPath.section].last!
        {
        case "Current Location Information", "Current Location Status":
            cell = tableView.dequeueReusableCell(withIdentifier: "cell_info", for: indexPath)
        case "Set Desired Accuracy":
            cell = tableView.dequeueReusableCell(withIdentifier: "cell_check", for: indexPath)
        case "Request Authorization", "Get Location Updates":
            cell = tableView.dequeueReusableCell(withIdentifier: "cell_button", for: indexPath)
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "cell_info", for: indexPath)
            break
        }
        
        cell.textLabel?.text = self.dataSource[indexPath.section][indexPath.row]
        
        switch self.dataSource[indexPath.section][indexPath.row]
        {
        case "Coordinate (Lat, Long)":
            cell.detailTextLabel?.text = String(describing: locationManager.location?.coordinate)
        case "Altitude (meters)":
            cell.detailTextLabel?.text = String(describing: locationManager.location?.altitude)
            
        case "Placemark name":
            cell.detailTextLabel?.text = currentPlacemark?.name
        case "Placemark ISOcountryCode":
            cell.detailTextLabel?.text = currentPlacemark?.isoCountryCode
        case "Placemark country":
            cell.detailTextLabel?.text = currentPlacemark?.country
        case "Placemark postalCode":
            cell.detailTextLabel?.text = currentPlacemark?.postalCode
        case "Placemark administrativeArea":
            cell.detailTextLabel?.text = currentPlacemark?.administrativeArea
        case "Placemark subAdministrativeArea":
            cell.detailTextLabel?.text = currentPlacemark?.subAdministrativeArea
        case "Placemark locality":
            cell.detailTextLabel?.text = currentPlacemark?.locality
        case "Placemark subLocality":
            cell.detailTextLabel?.text = currentPlacemark?.subLocality
        case "Placemark thoroughfare":
            cell.detailTextLabel?.text = currentPlacemark?.thoroughfare
        case "Placemark subThoroughfare":
            cell.detailTextLabel?.text = currentPlacemark?.subThoroughfare
        case "Placemark region":
            cell.detailTextLabel?.text = currentPlacemark?.region?.identifier
        case "Placemark timeZone":
            cell.detailTextLabel?.text = currentPlacemark?.timeZone?.identifier
        case "Placemark inlandWater":
            cell.detailTextLabel?.text = currentPlacemark?.inlandWater
        case "Placemark ocean":
            cell.detailTextLabel?.text = currentPlacemark?.ocean
        case "Placemark areasOfInterest":
            cell.detailTextLabel?.text = String(describing: currentPlacemark?.areasOfInterest)
            
        case "locationServicesEnabled":
            cell.detailTextLabel?.text = String(CLLocationManager.locationServicesEnabled())
        case "authorizationStatus":
            cell.detailTextLabel?.text = getAuthorizationStatusString(CLLocationManager.authorizationStatus())
        case "deferredLocationUpdatesAvailable":
            cell.detailTextLabel?.text = String(CLLocationManager.deferredLocationUpdatesAvailable())
        case "significantLocationChangeMonitoringAvailable":
            cell.detailTextLabel?.text = String(CLLocationManager.significantLocationChangeMonitoringAvailable())
        case "headingAvailable":
            cell.detailTextLabel?.text = String(CLLocationManager.headingAvailable())
        case "isRangingAvailable":
            cell.detailTextLabel?.text = String(CLLocationManager.isRangingAvailable())

        case "kCLLocationAccuracyBestForNavigation":
            cell.accessoryType = locationManager.desiredAccuracy ==  kCLLocationAccuracyBestForNavigation ? .checkmark : .none
        case "kCLLocationAccuracyBest":
            cell.accessoryType = locationManager.desiredAccuracy ==  kCLLocationAccuracyBest ? .checkmark : .none
        case "kCLLocationAccuracyNearestTenMeters":
            cell.accessoryType = locationManager.desiredAccuracy ==  kCLLocationAccuracyNearestTenMeters ? .checkmark : .none
        case "kCLLocationAccuracyHundredMeters":
            cell.accessoryType = locationManager.desiredAccuracy ==  kCLLocationAccuracyHundredMeters ? .checkmark : .none
        case "kCLLocationAccuracyKilometer":
            cell.accessoryType = locationManager.desiredAccuracy ==  kCLLocationAccuracyKilometer ? .checkmark : .none
        case "kCLLocationAccuracyThreeKilometers":
            cell.accessoryType = locationManager.desiredAccuracy ==  kCLLocationAccuracyThreeKilometers ? .checkmark : .none
            
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch self.dataSource[indexPath.section][indexPath.row]
        {
        case "requestWhenInUseAuthorization":
            locationManager.requestWhenInUseAuthorization()
        case "requestAlwaysAuthorization":
            locationManager.requestAlwaysAuthorization()
            
        case "kCLLocationAccuracyBestForNavigation":
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            tableView.reloadSections(IndexSet(integer: 3), with: .none)
        case "kCLLocationAccuracyBest":
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            tableView.reloadSections(IndexSet(integer: 3), with: .none)
        case "kCLLocationAccuracyNearestTenMeters":
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            tableView.reloadSections(IndexSet(integer: 3), with: .none)
        case "kCLLocationAccuracyHundredMeters":
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            tableView.reloadSections(IndexSet(integer: 3), with: .none)
        case "kCLLocationAccuracyKilometer":
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            tableView.reloadSections(IndexSet(integer: 3), with: .none)
        case "kCLLocationAccuracyThreeKilometers":
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            tableView.reloadSections(IndexSet(integer: 3), with: .none)

        case "startUpdatingLocation":
            locationManager.startUpdatingLocation()
        case "stopUpdatingLocation":
            locationManager.stopUpdatingLocation()
        case "requestLocation":
            locationManager.requestLocation()
        case "startMonitoringSignificantLocationChanges":
            locationManager.startMonitoringSignificantLocationChanges()
        case "stopMonitoringSignificantLocationChanges":
            locationManager.stopMonitoringSignificantLocationChanges()

            
        default:
            break
        }
    }

}
