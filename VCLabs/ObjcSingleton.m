//
//  ObjcSingleton.m
//  VCLabs
//
//  Created by Victor Chandra on 24/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

#import "ObjcSingleton.h"

@implementation ObjcSingleton

+ (id)sharedSingleton
{
    static ObjcSingleton *sharedSingleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[self alloc] init];
    });
    return sharedSingleton;
}
- (id)init
{
    if (self = [super init])
    {
        // Additional init
    }
    return self;
}
- (void)dealloc
{
    // Should never be called
}

@end