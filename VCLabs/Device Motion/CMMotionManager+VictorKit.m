//
//  CMMotionManager+VictorKit.m
//  VCLabs
//
//  Created by Victor Chandra on 28/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

#import "CMMotionManager+VictorKit.h"

@implementation CMMotionManager (VictorKit)

+ (CMMotionManager *)sharedManager
{
    static CMMotionManager *sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CMMotionManager alloc] init];
    });
    return sharedManager;
}

@end
