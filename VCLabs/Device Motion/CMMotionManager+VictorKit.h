//
//  CMMotionManager+VictorKit.h
//  VCLabs
//
//  Created by Victor Chandra on 28/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>

@interface CMMotionManager (VictorKit)

+ (CMMotionManager *)sharedManager;

@end
