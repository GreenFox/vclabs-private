//
//  NavMenuVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 2/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit
import RESideMenu

class NavMenuVC: UITableViewController, NavContentDelegate
{
    let arrayMenu = [
        "Navigation Bar",
        "Tab Bar",
        "Side Menu"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayMenu.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = arrayMenu[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch arrayMenu[indexPath.row]
        {
        case "Navigation Bar":
            openNavigationBar()
        case "Tab Bar":
            openTabBar()
        case "Side Menu":
            openSideMenu()
        default:
            break;
        }
    }
    
    // MARK: -
    
    func navContentClose(_ content: NavContentVC?, message: String?)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func openNavigationBar ()
    {
        let navContent = NavContentVC()
        navContent.navigationItem.title = "Navigation Bar"
        navContent.label.text = "This is Navigation Bar mechanism provided by Apple's UIKit"
        navContent.delegate = self
        let navMenuController = UINavigationController(rootViewController: navContent)
        present(navMenuController, animated: true, completion: nil)
    }

    func openTabBar ()
    {
        let navContentLeft = NavContentVC()
        navContentLeft.label.text = "Left Tab Bar. This is Tab Bar mechanism provided by Apple's UIKit"
        navContentLeft.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0)
        navContentLeft.delegate = self
        let navContentRight = NavContentVC()
        navContentRight.label.text = "Right Tab Bar. This is Tab Bar mechanism provided by Apple's UIKit"
        navContentRight.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 1)
        navContentRight.delegate = self
        
        let navMenuController = UITabBarController()
        navMenuController.viewControllers = [navContentLeft, navContentRight]
        present(navMenuController, animated: true, completion: nil)
    }
    
    func openSideMenu ()
    {
        let contentVC = SideMenuFirstVC()
        let navCon = UINavigationController(rootViewController: contentVC)
        let leftVC = SideMenuLeftVC()
        let rightVC = SideMenuRightVC()

        let reSideMenu = RESideMenu.init(contentViewController: navCon, leftMenuViewController: leftVC, rightMenuViewController: rightVC)
        reSideMenu?.backgroundImage = UIImage(named: "Stars")
        present(reSideMenu!, animated: true, completion: nil)
    }
}
