//
//  ObjcSingleton.h
//  VCLabs
//
//  Created by Victor Chandra on 24/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjcSingleton : NSObject

+ (id)sharedSingleton;

@end