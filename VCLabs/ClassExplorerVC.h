//
//  ClassBrowser.h
//  UltimateTool
//
//  Created by Victor Chandra on 3/5/13.
//  Copyright (c) 2013 ThreeMonkee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RuntimeHelper.h"


@interface ClassExplorerVC : UITableViewController

@property(nonatomic,retain) NSArray *arrayClass;

@end
