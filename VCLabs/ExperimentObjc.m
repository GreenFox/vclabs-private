//
//  ExperimentObjc.m
//  VCLabs
//
//  Created by Victor Chandra on 26/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import "ExperimentObjc.h"
#import "ObjcSingleton+VicAddition.h"

@implementation ExperimentObjc

- (void) doExperiment
{
    [[ObjcSingleton sharedSingleton] addedMethod];
}

@end
