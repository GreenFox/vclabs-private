//
//  BackendLessVC.m
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import "BackendLessVC.h"
#import <Backendless-ios-SDK/Backendless.h>
#import "BLComment.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "BLUser.h"
#import "VictorKit.h"


@interface BackendLessVC ()

@end

/*
 Section 0 : User
    Row 0 : Logged as    victor.chandra@coroma.com.au / Not logged
    Row 1 : Login / Logout
 
 Section 1 : Get Email
    Row 0 : Get own email
    Row 1 : Get all emails
 
 Request your own email
 Request all emails
 Request specific email
 
 
 Role : 
    System Administrator    : Read all, Write all
    Company Administrator   : Read all in own company, Write all in own company
    Employee                : Read all email, Write own email
    Standard User           : Read own email, Write own email
 */


@implementation BackendLessVC

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        self.title = @"BackendLess Demo";
        
        static NSString *appID = @"B8DBCB4E-6031-7046-FFC2-5D67FA59C800";
        static NSString *appSecret = @"49A38AAA-E5D7-702A-FF44-FC39215FFE00";
        static NSString *version = @"v1";
        [[Backendless sharedInstance] initApp:appID secret:appSecret version:version];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 2;
        
    } else if (section == 1)
    {
        return 2;
    }
    
    return 0;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"User";
        
    } else if (section == 1)
    {
        return @"Get Email";
    }
    
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"Footer";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0)
    {
        static NSString *CellIdentifier = @"CellUserInfo";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }
        [cell.textLabel setText:@"Signed in as"];
        [cell.detailTextLabel setText:(backendless.userService.currentUser ? backendless.userService.currentUser.email : @"Not signed in")];
        return cell;
        
    } else if (indexPath.section == 0 && indexPath.row == 1)
    {
        static NSString *CellIdentifier = @"CellSignInButton";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        [cell.textLabel setText:(backendless.userService.currentUser ? @"Sign Out" : @"Sign In")];
        return cell;
        
    }  else if (indexPath.section == 1 && indexPath.row == 0)
    {
        static NSString *CellIdentifier = @"CellGetMyEmail";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        [cell.textLabel setText:@"Get My Email"];
        return cell;
        
    }  else if (indexPath.section == 1 && indexPath.row == 1)
    {
        static NSString *CellIdentifier = @"CellGetAllEmail";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        [cell.textLabel setText:@"Get All Emails"];
        return cell;
        
    } else
    {
        return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DefaultCell"];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1)
    {
        if (backendless.userService.currentUser)
        {
            // Signed In. Do logout
            [UIAlertView bk_showAlertViewWithTitle:@"Sign Out ?" message:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"YES"] handler:^(UIAlertView *alertView, NSInteger buttonIndex)
            {
                if (buttonIndex == 1)
                {
                    [self signOut];
                }
            }];
            
        } else
        {
            // Signed Out. Do login
            // Sign In
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:(backendless.userService.currentUser ? @"Sign Out" : @"Sign In") message:nil delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
            [alertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
            [alertView bk_addButtonWithTitle:@"Confirm" handler:^
             {
                 [SVProgressHUD show];
                 [self signIn:[alertView textFieldAtIndex:0].text password:[alertView textFieldAtIndex:1].text];
             }];
            [alertView show];

        }
    } else if (indexPath.section == 1 && indexPath.row == 0)
    {
        // Get My Email
        [VictorKit alert:@"Your Email" message:(backendless.userService.currentUser ? backendless.userService.currentUser.email : @"N/A")];
        
//        id<IDataStore> dataStore = [backendless.persistenceService of:[BLUser class]];
//        BackendlessCollection *collection = [dataStore find:nil];
        
    } else if (indexPath.section == 1 && indexPath.row == 1)
    {
        // Get All Emails
        
        //[[Backendless sharedInstance].persistenceService save:[BLComment commentWithMessage:@"I'm in!" authorEmail:@"my.email@gmail.com"]];
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        BackendlessCollection *collection = [dataStore find:nil];
        NSLog(@"collection : %@", collection.totalObjects);
        
//        [backendless.persistenceService save:[Users aNewUser] response:^(id response)
//        {
//            [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"Success : %@", response]];
//
//        } error:^(Fault * fault)
//        {
//            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Error : %@", fault.message]];
//        }];
    }

}

- (void) signIn:(NSString*)username password:(NSString*)password
{
    [backendless.userService login:username password:password response:^(BackendlessUser *user)
    {
        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"Sign In Success : %@", user.email]];
        [self.tableView reloadData];
    } error:^(Fault *fault)
    {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Sign In Error : %@", fault.message]];
    }];
}

- (void) signOut
{
    [backendless.userService logout:^(id response)
     {
         [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"Sign Out Success"]];
         [self.tableView reloadData];
         
     } error:^(Fault *fault)
     {
         [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Sign Out Error : %@", fault.message]];
     }];
}

@end