//
//  LocaleExplorerVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 30/12/17.
//  Copyright © 2017 Victor. All rights reserved.
//

import UIKit

class LocaleExplorerVC: UITableViewController {

    /// Data source
    let availableLocaleIdentifiers = NSLocale.availableLocaleIdentifiers.sorted()
    let isoCountryCodes = NSLocale.isoCountryCodes.sorted()
    let isoLanguageCodes = NSLocale.isoLanguageCodes.sorted()
    let isoCurrencyCodes = NSLocale.isoCurrencyCodes.sorted()
    let commonISOCurrencyCodes = NSLocale.commonISOCurrencyCodes.sorted()
    let preferredLanguages = NSLocale.preferredLanguages.sorted()
    ///
    let sectionTitles = ["Available Locale Identifiers",
                         "ISO Country Codes",
                         "ISO Language Codes",
                         "ISO Currency Codes",
                         "Common ISO Currency Codes",
                         "Preferred Languages"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Locale Explorer"
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func compileMessage(locale: NSLocale) -> String {
        // Prepare
        let defaultValue = "N/A"
        // Fill values
        let _countryCode = locale.countryCode ?? defaultValue
        let _languageCode = locale.languageCode
        let _scriptCode = locale.scriptCode ?? defaultValue
        let _variantCode = locale.variantCode ?? defaultValue
        let _collationIdentifier = locale.collationIdentifier ?? defaultValue
        let _collatorIdentifier = locale.collatorIdentifier
        let _usesMetricSystem: Bool = locale.usesMetricSystem
        let _decimalSeparator = locale.decimalSeparator
        let _groupingSeparator = locale.groupingSeparator
        let _currencyCode = locale.currencyCode ?? defaultValue
        let _currencySymbol = locale.currencySymbol
        let _calendarIdentifier = locale.calendarIdentifier
        let _quotationBeginDelimiter = locale.quotationBeginDelimiter
        let _quotationEndDelimiter = locale.quotationEndDelimiter
        let _alternateQuotationBeginDelimiter = locale.alternateQuotationBeginDelimiter
        let _alternateQuotationEndDelimiter = locale.alternateQuotationEndDelimiter

        let string =
        "Country Code: \(_countryCode)" + "\n" +
        "Language Code: \(_languageCode)" + "\n" +
        "Script Code: \(_scriptCode)" + "\n" +
        "Variant Code: \(_variantCode)" + "\n" +
        "Collation Identifier Code: \(_collationIdentifier)" + "\n" +
        "Collator Identifier Code: \(_collatorIdentifier)" + "\n" +
        "Uses Metric System: \(_usesMetricSystem)" + "\n" +
        "Decimal Separator: \(_decimalSeparator)" + "\n" +
        "Grouping Separator: \(_groupingSeparator)" + "\n" +
        "Currency Code: \(_currencyCode)" + "\n" +
        "Currency Symbol: \(_currencySymbol)" + "\n" +
        "Calendar Identifier: \(_calendarIdentifier)" + "\n" +
        "Quotation Begin Delimiter: \(_quotationBeginDelimiter)" + "\n" +
        "Quotation End Delimiter: \(_quotationEndDelimiter)" + "\n" +
        "Alternate Quotation Begin Delimiter: \(_alternateQuotationBeginDelimiter)" + "\n" +
        "Alternate Quotation End Delimiter: \(_alternateQuotationEndDelimiter)"
        
        return string
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return availableLocaleIdentifiers.count
        case 1: return isoCountryCodes.count
        case 2: return isoLanguageCodes.count
        case 3: return isoCurrencyCodes.count
        case 4: return commonISOCurrencyCodes.count
        case 5: return preferredLanguages.count
        default: return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var text = ""
        switch indexPath.section {
        case 0:
            text = availableLocaleIdentifiers[indexPath.row]
        case 1:
            text = isoCountryCodes[indexPath.row]
        case 2:
            text = isoLanguageCodes[indexPath.row]
        case 3:
            text = isoCurrencyCodes[indexPath.row]
        case 4:
            text = commonISOCurrencyCodes[indexPath.row]
        case 5:
            text = preferredLanguages[indexPath.row]
        default: break
        }
        cell.textLabel?.text = text

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Only works on first section
        if indexPath.section != 0 { return }
        // Process it
        let identifier = availableLocaleIdentifiers[indexPath.row]
        let locale = NSLocale.init(localeIdentifier: identifier)
        // Prepare message
        let message = compileMessage(locale: locale)
        // Show as alert
        VictorKitSwift.alert(title: "Identifier: \(identifier)", message: message, context: self)
    }
    
    // MARK: - Indexing
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return ["Section 0", "Section 1", "Section 2", "Section 3", "Section 4", "Section 5"]
    }
}
