//
//  KinveyVC.m
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import "KinveyVC.h"
#import <KinveyKit/KinveyKit.h>

@interface KinveyVC ()

@end

@implementation KinveyVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initKinvey];
}

- (void) initKinvey
{
    [[KCSClient sharedClient] initializeKinveyServiceForAppKey:@"kid_Z1o5b4B46l"
                                                 withAppSecret:@"4fbb685b15724fe2a21a89b740c06aa3"
                                                  usingOptions:nil];
}

- (void) testKinvey
{
    [KCSPing pingKinveyWithBlock:^(KCSPingResult *result)
     {
         if (result.pingWasSuccessful)
         {
             NSLog(@"Kinvey Ping Success");
         } else {
             NSLog(@"Kinvey Ping Failed");
         }
     }];
}

@end
