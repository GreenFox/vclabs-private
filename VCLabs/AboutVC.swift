//
//  AboutVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 6/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit

class AboutVC: UIViewController
{
    @IBOutlet weak var textView: UITextView!
    
    let message = "G'day!\n\nThis project shows many things I can make with iOS SDK using Objective-C and Swift. Feel free to browse the showcase of my work, portfolio and libraries. I create them using my own libraries, some of them using Apple's sample codes, and some of them using third party libraries.\n\nIf you have any question or feedback, feel free to contact me through email below.\n\nCheers, Victor."
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        textView.text = message
    }
    
    @IBAction func contactMe(_ sender: AnyObject)
    {
        guard let url = URL(string: "mailto:admin@victorchandra.com") else { return }
//        UIApplication.shared.openURL(url!)
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
