//
//  Flat Color
//  Victor Chandra
//

import UIKit

extension UIColor
{
    @objc class func colorWithHex(_ hex: Int, alpha: CGFloat = 1.0) -> UIColor
    {
        let r = CGFloat((hex & 0xff0000) >> 16) / 255.0
        let g = CGFloat((hex & 0x00ff00) >>  8) / 255.0
        let b = CGFloat((hex & 0x0000ff) >>  0) / 255.0
        return UIColor(red: r, green: g, blue: b, alpha: alpha)
    }

    // green sea
    class func flatTurquoise()    -> UIColor { return UIColor.colorWithHex(0x1abc9c) }
    class func flatGreenSea()     -> UIColor { return UIColor.colorWithHex(0x16a085) }
    // green
    class func flatEmerald()      -> UIColor { return UIColor.colorWithHex(0x2ecc71) }
    class func flatNephritis()    -> UIColor { return UIColor.colorWithHex(0x27ae60) }
    // blue
    class func flatPeterRiver()   -> UIColor { return UIColor.colorWithHex(0x3498db) }
    class func flatBelizeHole()   -> UIColor { return UIColor.colorWithHex(0x2980b9) }
    // purple
    class func flatAmethyst()     -> UIColor { return UIColor.colorWithHex(0x9b59b6) }
    class func flatWisteria()     -> UIColor { return UIColor.colorWithHex(0x8e44ad) }
    // dark blue
    class func flatWetAsphalt()   -> UIColor { return UIColor.colorWithHex(0x34495e) }
    class func flatMidnightBlue() -> UIColor { return UIColor.colorWithHex(0x2c3e50) }
    // yellow
    class func flatSunFlower()    -> UIColor { return UIColor.colorWithHex(0xf1c40f) }
    class func flatOrange()   -> UIColor { return UIColor.colorWithHex(0xf39c12) }
    // orange
    class func flatCarrot()       -> UIColor { return UIColor.colorWithHex(0xe67e22) }
    class func flatPumkin()       -> UIColor { return UIColor.colorWithHex(0xd35400) }
    // red
    class func flatAlizarin()     -> UIColor { return UIColor.colorWithHex(0xe74c3c) }
    class func flatPomegranate()  -> UIColor { return UIColor.colorWithHex(0xc0392b) }
    // white
    class func flatClouds()       -> UIColor { return UIColor.colorWithHex(0xecf0f1) }
    class func flatSilver()       -> UIColor { return UIColor.colorWithHex(0xbdc3c7) }
    // gray
    class func flatAsbestos()     -> UIColor { return UIColor.colorWithHex(0x7f8c8d) }
    class func flatConcerte()     -> UIColor { return UIColor.colorWithHex(0x95a5a6) }
}
