//
//  BLComment.h
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLComment : NSObject

@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *authorEmail;

+(BLComment *)commentWithMessage:(NSString *)message authorEmail:(NSString *)authorEmail;

@end