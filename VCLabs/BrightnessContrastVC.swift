//
//  BrightnessContrastVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 13/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit
import CoreImage

class BrightnessContrastVC: UIViewController
{
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var sliderBrightness: UISlider!
    @IBOutlet weak var sliderContrast: UISlider!
    @IBOutlet weak var sliderSepia: UISlider!

    let context = CIContext(options:nil)
    let filterSepia = CIFilter(name: "CISepiaTone")!
    let filterColorControls = CIFilter(name: "CIColorControls")!
    let beginImage = CIImage(image:UIImage(named: "landscape")!)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Initial setup
        imageView.image = UIImage(named: "landscape")!
        self.title = "Core Image Sample"
    }
    
    func createImage() {
        // Do it on background thread
        DispatchQueue.global().async {
            // Pass through filter sepia
            self.filterSepia.setValue(self.beginImage, forKey: kCIInputImageKey)
            guard let outputImageOne = self.filterSepia.outputImage else { return }
            
            // Pass through filter color controls
            self.filterColorControls.setValue(outputImageOne, forKey: kCIInputImageKey)
            guard let outputImageTwo = self.filterColorControls.outputImage else { return }
            
            // Create image
            guard let finalCGImage = self.context.createCGImage(outputImageTwo, from: outputImageTwo.extent) else { return }
            
            // Finally go to main thread
            DispatchQueue.main.async {
                self.imageView.image = UIImage(cgImage: finalCGImage)
            }
        }
    }
    
    @IBAction func sliderBrightnessChanged(_ sender: AnyObject)
    {
        let sliderValue = (sender as! UISlider).value
        filterColorControls.setValue(sliderValue, forKey: kCIInputBrightnessKey)
        filterSepia.setValue(sliderSepia.value, forKey: kCIInputIntensityKey)
        filterColorControls.setValue(sliderContrast.value, forKey: kCIInputContrastKey)
        
        createImage()
    }
    
    @IBAction func sliderContrastChanged(_ sender: AnyObject)
    {
        let sliderValue = (sender as! UISlider).value
        filterColorControls.setValue(sliderValue, forKey: kCIInputContrastKey)
        filterSepia.setValue(sliderSepia.value, forKey: kCIInputIntensityKey)
        filterColorControls.setValue(sliderBrightness.value, forKey: kCIInputBrightnessKey)

        createImage()
    }
    
    @IBAction func sliderSepiaChanged(_ sender: AnyObject)
    {
        let sliderValue = (sender as! UISlider).value
        filterSepia.setValue(sliderValue, forKey: kCIInputIntensityKey)
        filterColorControls.setValue(sliderContrast.value, forKey: kCIInputContrastKey)
        filterColorControls.setValue(sliderBrightness.value, forKey: kCIInputBrightnessKey)

        createImage()
    }
}
