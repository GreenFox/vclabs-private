//
//  MapLocationVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 30/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import UIKit
import MapKit

class MapLocationVC: UIViewController, MKMapViewDelegate
{
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var label: UILabel!

    
    // MARK: - Override
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // MARK: - MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool)
    {
        
    }

    func mapViewWillStartLoadingMap(_ mapView: MKMapView)
    {
        
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView)
    {
        
    }
    
    func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error)
    {
        
    }

    func mapViewWillStartRenderingMap(_ mapView: MKMapView)
    {
        
    }
    
    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool)
    {
        
    }

    /*
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        
    }*/
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView])
    {
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView)
    {
        
    }
    
    func mapViewWillStartLocatingUser(_ mapView: MKMapView)
    {
        
    }
    
    func mapViewDidStopLocatingUser(_ mapView: MKMapView)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool)
    {
        
    }
    
    /*
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer
    {
        
    }*/
    
    func mapView(_ mapView: MKMapView, didAdd renderers: [MKOverlayRenderer])
    {
        
    }
}
