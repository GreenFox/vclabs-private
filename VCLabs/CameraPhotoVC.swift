//
//  CameraPhotoVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 30/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import UIKit
import MobileCoreServices

class CameraPhotoVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    let imagePicker: UIImagePickerController! = UIImagePickerController()
    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage:UIImage = (info[UIImagePickerControllerOriginalImage]) as? UIImage
        {
            //let selectorToCall = Selector("imageWasSavedSuccessfully:didFinishSavingWithError:context:")
            //UIImageWriteToSavedPhotosAlbum(pickedImage, self, selectorToCall, nil)
            imageView.image = pickedImage
        }
        imagePicker.dismiss(animated: true, completion: {
            // Anything you want to happen when the user saves an image
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: {
            // Anything you want to happen when the user selects cancel
        })
    }

    @IBAction func openCamera(_ sender : UIButton)
    {
        if (UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil
            {
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .camera
                imagePicker.cameraCaptureMode = .photo
                imagePicker.delegate = self
                present(imagePicker, animated: true, completion: {})
            } else
            {
                VictorKit.alertError("Rear camera doesn't exist. Application cannot access the camera.")
            }
        } else
        {
            VictorKit.alertError("Camera inaccessable. Application cannot access the camera.")
        }
    }

    @IBAction func openImageLibrary(_ sender : UIButton)
    {
        if (UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum))
        {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.delegate = self
            present(imagePicker, animated: true, completion: {})
        } else
        {
            VictorKit.alertError("Photo Album inaccessable. Application cannot access the photo album.")
        }
    }
    
    @IBAction func segmentChanged(_ sender: AnyObject)
    {
        // Remove any subview of Image View
        for subview in self.imageView.subviews
        {
            subview.removeFromSuperview()
        }
        
        let segmentedControl = sender as! UISegmentedControl
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            // Clean
            break
        case 1:
            // Extra Light Blur
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
            visualEffectView.frame = imageView.bounds
            imageView.addSubview(visualEffectView)
        case 2:
            // Light Blur
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            visualEffectView.frame = imageView.bounds
            imageView.addSubview(visualEffectView)
        case 3:
            // Dark Blur
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
            visualEffectView.frame = imageView.bounds
            imageView.addSubview(visualEffectView)
        default:
            break
        }
    }
    
}
