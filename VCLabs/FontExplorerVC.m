//
//  FontBrowser.m
//  UltimateTool
//
//  Created by Victor Chandra on 11/16/12.
//  Copyright (c) 2012 ThreeMonkee. All rights reserved.
//

#import "FontExplorerVC.h"

@interface FontExplorerVC ()
@property (nonatomic,strong) NSArray *arrayFontFamily;
@property (nonatomic,strong) NSDictionary *dictionaryFont;
@end

@implementation FontExplorerVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:TRUE];
        self.arrayFontFamily = [[UIFont familyNames] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        
        // Dict Font
        NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
        for (NSString *aFontFamily in self.arrayFontFamily)
            [tempDict setObject:[UIFont fontNamesForFamilyName:aFontFamily] forKey:aFontFamily];
        self.dictionaryFont = tempDict;
        
        self.title = @"Font Explorer";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.arrayFontFamily count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dictionaryFont[self.arrayFontFamily[section]] count];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.arrayFontFamily objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    NSString *fontName = [self.dictionaryFont objectForKey:self.arrayFontFamily[indexPath.section]][indexPath.row];
    [cell.textLabel setText:fontName];
    [cell.textLabel setFont:[UIFont fontWithName:fontName size:16]];
    
    return cell;
}

@end
