//
//  PortfolioGalleryVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 6/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit

class PortfolioGalleryVC: UIViewController
{
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var container: UIScrollView!
 
    override func viewDidLoad()
    {
        super.viewDidLoad()

        setupGallery()
    }
    
    func setupGallery()
    {
        let images = [
            UIImage(named: "p1"),
            UIImage(named: "p2"),
            UIImage(named: "p3"),
            UIImage(named: "p4"),
            UIImage(named: "p5"),
            UIImage(named: "p6"),
            UIImage(named: "p7"),
            UIImage(named: "p8"),
            UIImage(named: "p9"),
            UIImage(named: "p10"),
            UIImage(named: "p11"),
            UIImage(named: "p12"),
            UIImage(named: "p13"),
            UIImage(named: "p14"),
            UIImage(named: "p15"),
            UIImage(named: "p16"),
            UIImage(named: "p17"),
            UIImage(named: "p18"),
            UIImage(named: "p19"),
            UIImage(named: "p20"),
            UIImage(named: "p21"),
            UIImage(named: "p22")
        ]
        let rect = container.frame
        
        for i in 0  ..< images.count 
        {
            let imageView = UIImageView (frame: CGRect(x: CGFloat(i) * rect.width, y: 0, width: rect.width, height: rect.height))
            imageView.image = images[i]
            imageView.clipsToBounds = true
            imageView.contentMode = .scaleAspectFit
            container.addSubview(imageView)
            container.contentSize = CGSize(width: CGFloat(i) * rect.width + rect.width, height: rect.height)
            container.addSubview(imageView)
        }
    }
}
