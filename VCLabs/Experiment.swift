//
//  Experiment.swift
//  VCLabs
//
//  Created by Victor Chandra on 26/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit

class Experiment: NSObject
{
    func doExperiment ()
    {
        let _ = SwiftSingleton.getRandom(20)
        
        let _ = SwiftSingleton.aNewStaticMethod()
    }
}
