//
//  CallMessagingVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 31/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import UIKit
import MessageUI

class CallMessagingVC: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate
{

    @IBOutlet weak var inputPhone: UITextField!
    @IBOutlet weak var inputEmail: UITextField!
    
    
    // MARK: - Override
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // MARK: - Delegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        switch result
        {
        case MessageComposeResult.cancelled:
            VictorKit.alert("Sending Cancelled", message: nil)
        case MessageComposeResult.failed:
            VictorKit.alert("Sending Failed", message: nil)
        case MessageComposeResult.sent:
            VictorKit.alert("Sending Successful", message: nil)
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true, completion: nil)
    }

    
    // MARK: - IBAction
    
    @IBAction func sendSMS(_ sender: AnyObject)
    {
        if (!MFMessageComposeViewController.canSendText())
        {
            VictorKit.alertError("Device cannot send text")
            return;
        }
        
        if inputPhone.text?.length == 0
        {
            VictorKit.alertError("Please input a phone number")
            return;
        }
        
        let messageVC = MFMessageComposeViewController()
        messageVC.body = "Hello";
        messageVC.recipients = [inputPhone.text!]
        messageVC.messageComposeDelegate = self;
        self.present(messageVC, animated: true, completion: nil)
    }
    
    @IBAction func callPhone(_ sender: AnyObject)
    {
        if !SwiftSingleton.canMakeCall()
        {
            VictorKit.alertError("Device cannot make call")
            return
        }
        
        if inputPhone.text?.length == 0
        {
            VictorKit.alertError("Please input a phone number")
            return;
        }
        
        SwiftSingleton.callPhone(inputPhone.text!)
    }
    
    @IBAction func emailPopup(_ sender: AnyObject)
    {
        if !MFMailComposeViewController.canSendMail()
        {
            VictorKit.alertError("Device cannot send email")
            return
        }
        
        if inputEmail.text?.length == 0
        {
            VictorKit.alertError("Please input an email address")
            return;
        }

        let mailComposeViewController = configuredMailComposeViewController(inputEmail.text!)
        self.present(mailComposeViewController, animated: true, completion: nil)
    }
    
    @IBAction func emailMailApp(_ sender: AnyObject)
    {
        if inputEmail.text?.length == 0
        {
            VictorKit.alertError("Please input an email address")
            return;
        }

        guard let url = URL(string: "mailto:\(String(describing: inputEmail.text))") else { return }
//        UIApplication.shared.openURL(url)
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // MARK: - Custom

    func configuredMailComposeViewController(_ emailAddress: String) -> MFMailComposeViewController
    {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([emailAddress])
        mailComposerVC.setSubject("Hello")
        mailComposerVC.setMessageBody("Sending email with Swift is fun!", isHTML: false)
        
        return mailComposerVC
    }
}
