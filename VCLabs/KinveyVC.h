//
//  KinveyVC.h
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KinveyVC : UIViewController

- (void) initKinvey;
- (void) testKinvey;

@end
