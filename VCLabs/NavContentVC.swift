//
//  NavContentVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 2/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit
import SWFrameButton

protocol NavContentDelegate
{
    func navContentClose (_ content:NavContentVC?, message:String?)
}

class NavContentVC: UIViewController
{
    var delegate:NavContentDelegate! = nil
    var label:UILabel = UILabel()

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        let closeButton = SWFrameButton(frame: CGRect(x: 0,y: 0,width: 84,height: 32))
        closeButton.setTitle("Close", for: UIControlState())
        closeButton.addTarget(self, action: #selector(NavContentVC.close), for: .touchUpInside)
        closeButton.center = UIApplication.shared.keyWindow!.center
        self.view.addSubview(closeButton)
        
        label.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow!.frame.size.width - 20, height: 88)
        label.center = CGPoint(x: closeButton.center.x, y: closeButton.center.y / 2)
        label.textAlignment = .center
        label.numberOfLines = 0
        self.view.addSubview(label)
    }
    
    @objc func close ()
    {
        delegate!.navContentClose(self, message: nil)
    }
}
