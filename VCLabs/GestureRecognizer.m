//
//  GestureRecognizer.m
//  UltimateTool
//
//  Created by Victor Chandra on 1/22/13.
//  Copyright (c) 2013 ThreeMonkee. All rights reserved.
//

#import "GestureRecognizer.h"
#import "VictorKit.h"

@interface GestureRecognizer ()
@property (nonatomic, retain) UILabel *label;
@end

@implementation GestureRecognizer

-(IBAction)singleTapAction:(id)sender
{
    QVLog;
    
    [self updateLabel:@"Single Tap"];
}

-(IBAction)doubleTapAction:(id)sender
{
    QVLog;

    [self updateLabel:@"Double Tap"];
}

-(IBAction)pinchAction:(id)sender
{
    QVLog;

    [self updateLabel:@"Pinch"];
}

-(IBAction)rotationAction:(id)sender
{
    QVLog;
    
    [self updateLabel:@"Rotation"];
}

-(IBAction)swipeAction:(id)sender
{
    QVLog;

    [self updateLabel:@"Swipe"];
}

-(IBAction)panAction:(id)sender
{
    QVLog;

    [self updateLabel:@"Pan"];
}

-(IBAction)longPressAction:(id)sender
{
    QVLog;

    [self updateLabel:@"Long Press"];
}

#pragma mark - 

- (void) updateLabel: (NSString*) string {
    // Update
    self.label.text = string;
    [self.label sizeToFit];
    self.label.center = self.view.center;
}

- (void) addGestureRecongnizers
{
    // Single Tap
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapAction:)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setEnabled:TRUE];
    [self.view addGestureRecognizer:singleTap];
    
    // Double Tap
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapAction:)];
    [doubleTap setNumberOfTapsRequired:2];
    [doubleTap setEnabled:TRUE];
    [self.view addGestureRecognizer:doubleTap];
    
    // Pinch
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchAction:)];
    [pinch setEnabled:TRUE];
    [self.view addGestureRecognizer:pinch];
    
    // Rotation
    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationAction:)];
    [rotate setEnabled:TRUE];
    [self.view addGestureRecognizer:rotate];
    
    // Swipe
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
    [swipe setEnabled:TRUE];
    [self.view addGestureRecognizer:swipe];
    
    // Pan
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panAction:)];
    [pan setEnabled:TRUE];
    [self.view addGestureRecognizer:pan];
    
    // Long Press
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
    [longPress setEnabled:TRUE];
    [self.view addGestureRecognizer:longPress];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Gesture Recognizers";
    [self.view setBackgroundColor:[UIColor whiteColor]];

    // Add a label
    self.label = [[UILabel alloc] init];
    self.label.text = @"Test your gesture here...";
    [self.label sizeToFit];
    self.label.center = self.view.center;
    [self.view addSubview:self.label];
    
    // Add gesture recognizer
    [self addGestureRecongnizers];
}

@end
