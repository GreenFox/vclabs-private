//
//  ClassDetail.h
//  UltimateTool
//
//  Created by Victor Chandra on 3/6/13.
//  Copyright (c) 2013 ThreeMonkee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface ClassDetailVC : UITableViewController

@property (nonatomic,retain) NSDictionary *dictThisClass;

@end
