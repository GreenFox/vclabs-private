//
//  CALayer+CALayer_WiggleAnimationAdditions.h
//  CoreAnimationFunHouse
//
//  Created by Brian Coyner on 10/8/11.
//  Copyright (c) 2011 Brian Coyner. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
@import UIKit;
@import Foundation;

@interface CALayer (WiggleAnimationAdditions)

- (void)bts_startWiggling;

- (void)bts_stopWiggling;

@end
