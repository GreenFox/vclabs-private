//
//  Copyright (c) 2011 Brian Coyner. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import <stdio.h>

void BTSPrintCurrentCTM(CGContextRef context);

void BTSDrawPoint(CGContextRef context, CGPoint point);

void BTSDrawCoordinateAxes(CGContextRef context);