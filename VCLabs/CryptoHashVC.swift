//
//  CryptoHashVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit

class CryptoHashVC: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var labelMD5: UILabel!
    @IBOutlet weak var labelSHA256: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Crypto Hash"
        textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.doHashing(textField)
        return true;
    }
    
    @IBAction func doHashing(_ sender: AnyObject)
    {
        if (textField.text?.length == 0)
        {
            VictorKit.alertError("Message empty")
            return
        }
        
        labelMD5.text = "MD5: \(textField.text!.md5()!)"
        labelSHA256.text = "SHA256: \(textField.text!.sha256()!)"
    }
}
