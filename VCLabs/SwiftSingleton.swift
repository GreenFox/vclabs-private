//
//  SwiftSingleton.swift
//  VCLabs
//
//  Created by Victor Chandra on 25/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import Foundation

class SwiftSingleton
{
    static let sharedInstance = SwiftSingleton()
    fileprivate init() {}
    
    static func canMakeCall () -> Bool
    {
        return UIApplication.shared.canOpenURL(URL(string: "tel://")!)
    }
    
    static func callPhone (_ phoneNumber: String)
    {
//        UIApplication.shared.openURL(URL(string: "tel://\(phoneNumber)")!)
        guard let url = URL(string: "tel://\(phoneNumber)") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // Standard random
    static func getRandom (_ seed:Int) -> Int
    {
        return Int(arc4random_uniform(UInt32(seed)))
    }
    
    // Ex: getRandomWeighted (probabilities: [9, 4, 1, 8, 3, 7, 5])
    static func getRandomWeighted (_ probabilities: [Double]) -> Int
    {
        // Sum of all probabilities
        let sum = probabilities.reduce(0, +)
        // Random number in the range 0.0 <= rnd < sum :
        let rnd = sum * Double(arc4random_uniform(UInt32.max)) / Double(UInt32.max)
        // Find the first interval of accumulated probabilities into which `rnd` falls:
        var accum = 0.0
        for (i, p) in probabilities.enumerated()
        {
            accum += p
            if rnd < accum {
                return i
            }
        }
        // This point might be reached due to floating point inaccuracies:
        return (probabilities.count - 1)
    }

}

extension NSObject
{
    var theClassName: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}
