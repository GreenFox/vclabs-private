//
//  BluetoothLabVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 2/03/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit
import CoreBluetooth
import SVProgressHUD

class BluetoothLabVC: UIViewController, UITableViewDataSource, UITableViewDelegate, CBCentralManagerDelegate, CBPeripheralDelegate
{
    @IBOutlet weak var tableView: UITableView!
    lazy var myCentralManager : CBCentralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
    var allDevices: [Dictionary<String, AnyObject>] = []
    
    
    // MARK: - Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.setBackgroundColor(UIColor.lightGray)
//        SVProgressHUD.setOffsetFromCenter(UIOffsetMake(0, -108))
        
        // Prepare manager so it recognizes the state, not "unknown" anymore
        let _ = myCentralManager.state
        
        self.title = "Bluetooth Lab"
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        stopSearch(self)
//        self.stopSearch([])
    }
    
    // MARK: - IBAction
    
    @IBAction func startSearch(_ sender: AnyObject)
    {
        if myCentralManager.state == .poweredOn
        {
            print ("doing search.. \(myCentralManager)")
            SVProgressHUD.show(withStatus: "Searching");
            myCentralManager.scanForPeripherals(withServices: nil, options: nil)
            allDevices = []
        } else
        {
            VictorKit.alertError("Please make sure Bluetooth is on and ready")
        }
    }
    
    @IBAction func stopSearch(_ sender: AnyObject)
    {
        SVProgressHUD.dismiss()
        myCentralManager.stopScan()
    }
    
    
    // MARK: - CBCentralManagerDelegate
    
    func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        print ("centralManagerDidUpdateState")
    }
    
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any])
    {
        print ("centralManager willRestoreState")
        
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
    {
        print ("\nFound peripheral: \(peripheral)")
        print ("advertisementData: \(advertisementData)")
        print ("RSSI: \(RSSI)")
        print ("Peripheral name: \(String(describing: peripheral.name))")
        print ("Peripheral services: \(String(describing: peripheral.services))")
        print ("Peripheral state: \(peripheral.state)")
        print ("Peripheral indentifier: \(peripheral.identifier.uuidString)")
        
        self.allDevices.insert(["peripheral": peripheral, "advertisementData": advertisementData as AnyObject, "RSSI": RSSI], at: 0)
        self.tableView.reloadData()

    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral)
    {
        print ("didConnectPeripheral")

        peripheral.delegate = self
        peripheral.discoverServices(nil)
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?)
    {
        print ("centralManager didFailToConnectPeripheral")
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?)
    {
        print ("centralManager didDisconnectPeripheral")
    }
    
    
    // MARK: - CBPeripheralDelegate
    
    func peripheralDidUpdateName(_ peripheral: CBPeripheral)
    {
        print ("peripheralDidUpdateName")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService])
    {
        print ("peripheral didModifyServices")
    }
    
    func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?)
    {
        print ("peripheralDidUpdateRSSI")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?)
    {
        print ("peripheral didReadRSSI")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?)
    {
        let services = peripheral.services as [CBService]!
        print ("peripheral didDiscoverServices")
        
        for aService in services!
        {
            print ("service: \(aService)")
            print ("characteristics: \(String(describing: aService.characteristics))")
            print ("includedServices: \(String(describing: aService.includedServices))")
            
            peripheral.discoverCharacteristics(nil, for: aService)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?)
    {
        print ("peripheral didDiscoverIncludedServicesForService")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?)
    {
        print ("didDiscoverCharacteristicsForService : \(service)")
        
        for characteristic in service.characteristics!
        {
            print ("characteristic UUID : \(characteristic.uuid). Value: \(String(describing: characteristic.value)). Properties: \(characteristic.properties). Descriptors: \(String(describing: characteristic.descriptors))")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?)
    {
        print ("peripheral didUpdateValueForCharacteristic")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?)
    {
        print ("peripheral didWriteValueForCharacteristic")
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?)
    {
        print ("peripheral didUpdateNotificationStateForCharacteristic")
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?)
    {
        print ("peripheral didDiscoverDescriptorsForCharacteristic")
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?)
    {
        print ("peripheral didUpdateValueForDescriptor")
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?)
    {
        print ("peripheral didWriteValueForDescriptor")
        
    }
    
    
    // MARK: - Table View Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.allDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        // Configure the cell...
        let anObject = self.allDevices[indexPath.row]
        let peripheral = anObject["peripheral"] as! CBPeripheral
        //let advertisementData = anObject["advertisementData"]
        let rssi = anObject["RSSI"] as! NSNumber
        cell.textLabel?.text = "UUID \(peripheral.identifier.uuidString)"
        cell.detailTextLabel?.text = "RSSI \(rssi) dB"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let anObject = self.allDevices[indexPath.row]
        let peripheral = anObject["peripheral"] as! CBPeripheral
        _ = anObject["advertisementData"]
        _ = anObject["RSSI"] as! NSNumber
        let identifier = peripheral.identifier.uuidString
        _ = ""

        let alert = UIAlertView(title: "Peripheral", message: identifier, delegate: nil, cancelButtonTitle: "Cancel", otherButtonTitles: "Connect")
        alert.show { (alertView, index) -> Void in
            
            if index == 1
            {
                peripheral.discoverServices(nil)
                peripheral.delegate = self
                self.myCentralManager.connect(peripheral, options: nil)
                print ("connecting peripheral...")
            }
        }
        
//        UIAlertView.showWithTitle("Peripheral", message: identifier, style: .Default, cancelButtonTitle: "Cancel", otherButtonTitles: ["Connect"]) { (alertView, index) -> Void in
//            
//        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return "Found \(allDevices.count) device(s)"
    }
}
