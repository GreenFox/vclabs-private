//
//  UserDefaultsVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 30/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import UIKit

class UserDefaultsVC: UIViewController
{
    @IBOutlet weak var saveInput: UITextField!
    @IBOutlet weak var keyInput: UITextField!
    @IBOutlet weak var loadInput: UITextField!
    @IBOutlet weak var loadOutput: UILabel!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "User Defaults"
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        saveInput.resignFirstResponder()
        keyInput.resignFirstResponder()
        loadInput.resignFirstResponder()
    }

    
    // MARK: - IBAction
    
    @IBAction func doSave(_ sender: AnyObject)
    {
        if saveInput.text?.length == 0 || keyInput.text?.length == 0
        {
            VictorKit.alertError("Please fill in the value and key to save")
            return
        }
        
        UserDefaults.standard.set(saveInput.text, forKey: keyInput.text!)
        VictorKit.alert("Success", message: "Value \"\(saveInput.text!)\" saved for key \"\(keyInput.text!)\"")
    }
    
    @IBAction func doLoad(_ sender: AnyObject)
    {
        if loadInput.text?.length == 0
        {
            VictorKit.alertError("Please fill in the key to load")
            return
        }
        
        let theValue = UserDefaults.standard.object(forKey: loadInput.text!)
        if theValue != nil
        {
            loadOutput.text = "Value: \(theValue as! String)"
        } else
        {
            VictorKit.alertError("No value found for key \"\(loadInput.text!)\"")
        }
    }
    
}
