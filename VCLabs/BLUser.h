//
//  BLUser.h
//  VCLabs
//
//  Created by Victor Chandra on 1/02/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLUser : NSObject

@property (nonatomic, strong) NSString *objectId;

@end
