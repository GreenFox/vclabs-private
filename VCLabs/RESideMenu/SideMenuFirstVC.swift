//
//  SideMenuFirstVC.swift
//  VCLabs
//
//  Created by Victor Chandra on 3/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import UIKit

class SideMenuFirstVC: NavContentVC
{
    override func viewDidLoad()
    {
        self.title = "First Controller"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .plain, target: self, action: Selector(("presentLeftMenuViewController:")))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Right", style: .plain, target: self, action: Selector(("presentRightMenuViewController:")))
        
        let imageView = UIImageView(frame: self.view.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        imageView.image = UIImage(named: "Baloon")
        self.view.addSubview(imageView)
     
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
    }
    
    override func close()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
