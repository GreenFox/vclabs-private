//
//  DEMORightMenuViewController.h
//  RESideMenuExample
//
//  Created by Roman Efimov on 2/11/14.
//  Copyright (c) 2014 Roman Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu/RESideMenu.h>

@interface SideMenuRightVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
