//
//  ClassDetail.m
//  UltimateTool
//
//  Created by Victor Chandra on 3/6/13.
//  Copyright (c) 2013 ThreeMonkee. All rights reserved.
//

#import "ClassDetailVC.h"

typedef enum
{
    PROPERTY,
    PROTOCOL,
    CLASS_METHOD,
    INSTANCE_METHOD,
}CLASS_DETAIL;

@interface ClassDetailVC ()

@end

@implementation ClassDetailVC

#pragma mark -

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = [_dictThisClass objectForKey:@"name"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case PROPERTY:
            return [[_dictThisClass objectForKey:@"property"] count];
            break;
        case PROTOCOL:
            return [[_dictThisClass objectForKey:@"protocol"] count];
            break;
        case CLASS_METHOD:
            return [[_dictThisClass objectForKey:@"class_method"] count];
            break;
        case INSTANCE_METHOD:
            return [[_dictThisClass objectForKey:@"instance_method"] count];
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell.textLabel setNumberOfLines:2];
        [cell.textLabel setMinimumScaleFactor:0.5];
        [cell.textLabel setAdjustsFontSizeToFitWidth:TRUE];
    }
    
    // Configure the cell...
    switch (indexPath.section)
    {
        case PROPERTY:
            [cell.textLabel setText:[[_dictThisClass objectForKey:@"property"] objectAtIndex:indexPath.row]];
            break;
        case PROTOCOL:
            [cell.textLabel setText:[[_dictThisClass objectForKey:@"protocol"] objectAtIndex:indexPath.row]];
            break;
        case CLASS_METHOD:
            [cell.textLabel setText:[[_dictThisClass objectForKey:@"class_method"] objectAtIndex:indexPath.row]];
            break;
        case INSTANCE_METHOD:
            [cell.textLabel setText:[[_dictThisClass objectForKey:@"instance_method"] objectAtIndex:indexPath.row]];
            break;
        default:
            return 0;
            break;
    }
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case PROPERTY:
            return [NSString stringWithFormat:@"Property (%lu)", [[_dictThisClass objectForKey:@"property"] count]];
            break;
        case PROTOCOL:
            return [NSString stringWithFormat:@"Protocol (%lu)", [[_dictThisClass objectForKey:@"protocol"] count]];
            break;
        case CLASS_METHOD:
            return [NSString stringWithFormat:@"Class Methods (%lu)", [[_dictThisClass objectForKey:@"class_method"] count]];
            break;
        case INSTANCE_METHOD:
            return [NSString stringWithFormat:@"Instance Methods (%lu)", [[_dictThisClass objectForKey:@"instance_method"] count]];
            break;
        default:
            return 0;
            break;
    }

}

@end
