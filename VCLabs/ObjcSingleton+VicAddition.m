//
//  ObjcSingleton+VicAddition.m
//  VCLabs
//
//  Created by Victor Chandra on 26/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import "ObjcSingleton+VicAddition.h"

@implementation ObjcSingleton (VicAddition)

- (NSString*) addedMethod
{
    return @"Hello";
}

@end
