//
//  CMCar.h
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import <CloudMine/CloudMine.h>

@interface CMCar : CMObject

@property (strong, nonatomic) NSString *make;
@property (strong, nonatomic) NSString *model;
@property (nonatomic) NSInteger year;

@end