//
//  CMCar.m
//  VCLabs
//
//  Created by Victor Chandra on 31/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

#import "CMCar.h"

@implementation CMCar

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_make forKey:@"make"];
    [aCoder encodeObject:_model forKey:@"model"];
    [aCoder encodeInteger:_year forKey:@"year"];
}

- (id)initWithCoder:(NSCoder *)aCoder {
    if ((self = [super initWithCoder:aCoder])) {
        _make = [aCoder decodeObjectForKey:@"make"];
        _model = [aCoder decodeObjectForKey:@"model"];
        _year = [aCoder decodeIntegerForKey:@"year"];
    }
    return self;
}

@end