//
//  VictorKitSwift.swift
//  VCLabs
//
//  Created by Victor Chandra on 30/12/17.
//  Copyright © 2017 Victor. All rights reserved.
//

import Foundation

class VictorKitSwift {
    
    /**
     Show alert
     */
    public static func alert(title: String, message: String, context: UIViewController) {
        // Prepare
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        // Show on main thread
        DispatchQueue.main.async {
            context.present(alert, animated: true, completion: nil)
        }
    }
}
