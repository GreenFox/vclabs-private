//
//  VictorKit.h
//  VCLabs
//
//  Created by Victor Chandra on 23/05/2014.
//  Copyright (c) 2014 ThreeMonkee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#pragma mark - Sizes
// {540, 620} ModalPresentationStyleFormSheet

#pragma mark - Defines

#ifdef DEBUG
#define DebugLog( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#endif

#ifdef DEBUG
#define AlertDebug(s) [VictorKit alertDebug:[NSString stringWithFormat:@"%s [Line %d] : %@", __PRETTY_FUNCTION__, __LINE__, s.localizedDescription]]
#else
#define AlertDebug(s)
#endif

#define VCLog(x)        NSLog(@"%s [Line %d] %@ ", __PRETTY_FUNCTION__, __LINE__, x)
#define QVLog           NSLog(@"%s [Line %d]", __PRETTY_FUNCTION__, __LINE__)
#define QVCallStack     NSLog(@"Call Stack: %@", [NSThread callStackSymbols]);

#define MY_APPDELEGATE      (AppDelegate*)[[UIApplication sharedApplication] delegate]
#define PATH_DOCUMENTS      [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define PATH_TEMP           NSTemporaryDirectory()
#define PATH_HOME           NSHomeDirectory()
#define PATH_RESOURCE       [[NSBundle mainBundle] resourcePath]

#define RANDOM_NUMBER(MAX)  arc4random() % (MAX+1);

#define COLOR_RGB(r,g,b)    [UIColor colorWithRed:(float)r/255.0 green:(float)g/255.0 blue:(float)b/255.0 alpha:1.0]
#define COLOR_RGBA(r,g,b,a) [UIColor colorWithRed:(float)r/255.0 green:(float)g/255.0 blue:(float)b/255.0 alpha:(float)a]

#define isPhone             ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define isPad               ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define isDOrientationUnknown   [[UIDevice currentDevice] orientation] == UIDeviceOrientationUnknown
#define isDPortraitNormal       [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait
#define isDOrientationDefault   isDPortraitNormal
#define isDPortraitUpsideDown   [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown
#define isDPortrait             isDPortraitNormal || isDPortraitUpsideDown
#define isDLandscapeLeft        [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft
#define isDLandscapeRight       [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight
#define isDLandscape            isDLandscapeLeft || isDLandscapeRight
#define isDFaceUp               [[UIDevice currentDevice] orientation] == UIDeviceOrientationFaceUp
#define isDFaceDown             [[UIDevice currentDevice] orientation] == UIDeviceOrientationFaceDown

#define isUIPortraitNormal      [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait
#define isUIOrientationDefault  isUIPortraitNormal
#define isUIPortraitUpsideDown  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown
#define isUIPortrait            isUIPortraitNormal || isUIPortraitUpsideDown
#define isUILandscapeLeft       [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft
#define isUILandscapeRight      [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight
#define isUILandscape           isUILandscapeLeft || isUILandscapeRight

// Screen Related
// is it iPhone with retina 4 inches (iPhone 5)?
#define isRetina4 (isPhone && ([UIScreen mainScreen].bounds.size.height == 568))
#define isRetina ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))
#define screenSize [UIScreen mainScreen].bounds.size

// Checking an object
#define isNonZeroLength(object)     ([object respondsToSelector:@selector(length)] && ([object length] > 0))
#define isNonZeroData(object)       (isClassOfObject([NSData class], object) && isNonZeroLength(object))
#define isNonZeroString(object)     (isClassOfObject([NSString class], object) && isNonZeroLength(object))

#define isNonZeroCount(object)      ([object respondsToSelector:@selector(count)] && ([object count] > 0))
#define isNonZeroArray(object)      (isClassOfObject([NSArray class], object) && isNonZeroCount(object))
#define isNonZeroDictionary(object) (isClassOfObject([NSDictionary class], object) && isNonZeroCount(object))

// Time constants
#define VC_MINUTE   60
#define VC_HOUR     (60 * VC_MINUTE)
#define VC_DAY      (24 * VC_HOUR)
#define VC_5_DAYS   (5 * VC_DAY)
#define VC_WEEK     (7 * VC_DAY)
#define VC_MONTH    (30 * VC_DAY)
#define VC_YEAR     (365 * VC_DAY)

// Math
#define RADIANS(degrees) degrees / 57.2958f
#define GOLDEN_RATIO 1.61803398875
#define DEGREES_TO_RADIANS(d) (d * M_PI / 180)

// Color
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)(((rgbValue) & 0xFF0000) >> 16))/255.0 green:((float)(((rgbValue) & 0xFF00) >> 8))/255.0 blue:((float)((rgbValue) & 0xFF))/255.0 alpha:1.0]


// C Functions
CGPoint midPoint(CGRect r);
CATransform3D CATransform3DMakePerspective(CGFloat z);

#pragma mark - VictorKit

@interface VictorKit : NSObject

+ (void) phoneCallTo: (NSString*)phoneNumber;
+ (void) openAppStore: (NSString*)app;

+ (void) alertDebug: (NSString*)message;
+ (void) alertError: (NSString*)message;
+ (void) alert: (NSString*)title
       message: (NSString*)message;

+ (CGSize)makeSize: (CGSize)originalSize
         fitInSize: (CGSize)boxSize;
+ (CGSize)makeSize: (CGSize)originalSize
        fillInSize: (CGSize)boxSize;

+ (NSString*) appName;
+ (NSString*) appVersionShort;
+ (NSString*) appVersionLong;
+ (NSString*) operatingSystemVersion;
+ (NSString*) deviceModel;
+ (NSString*) bundleIdentifier;

+ (UIImage *)radialGradientImage: (CGSize)size
                           start: (float)start
                             end: (float)end
                          centre: (CGPoint)centre
                          radius: (float)radius;

+ (NSArray*) arrayOfCountryNames;
+ (void) logAllFonts;
+ (void) logAllLocales;
+ (void) logAllUnicodes;

+ (NSString *)extractYoutubeID:(NSString *)youtubeURL;

+ (BOOL) arrayOfString: (NSArray*)array
        containsString: (NSString*)string;

+ (BOOL) isEmail: (NSString*)candidate;

+ (BOOL) isContentOfDictionary: (NSDictionary *)etalonDictionary
              partOfDictionary: (NSDictionary *)dictionary;

+ (NSArray*) uniqueArrayFromArray: (NSArray*)array;

+ (UIImage*) screenshotOfView: (UIView*)view;

+ (NSDictionary*) cleanDictionary:(NSDictionary*)dictionary;
+ (NSArray*) cleanArray:(NSArray*)array;

@end



#pragma mark - UIButton+VictorKit

@interface UIButton (VictorKit)
#define kUIButtonBlockTouchUpInside @"TouchInside"
@property(nonatomic, strong) NSMutableDictionary *actions;
- (void)setAction:(NSString *)action withBlock:(void (^)(void))block;
@end


#pragma mark - UIAlertView+VictorKit

@interface UIAlertView (VictorKit)

typedef void (^UIAlertViewBlock) (UIAlertView *alertView);
typedef void (^UIAlertViewCompletionBlock) (UIAlertView *alertView, NSInteger buttonIndex);

+ (instancetype)showWithTitle:(NSString *)title
                      message:(NSString *)message
                        style:(UIAlertViewStyle)style
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSArray *)otherButtonTitles
                     tapBlock:(UIAlertViewCompletionBlock)tapBlock;

+ (instancetype)showWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSArray *)otherButtonTitles
                     tapBlock:(UIAlertViewCompletionBlock)tapBlock;

- (void)showWithCompletion:(UIAlertViewCompletionBlock)completionBlock;

@property (copy, nonatomic) UIAlertViewCompletionBlock tapBlock;
@property (copy, nonatomic) UIAlertViewCompletionBlock willDismissBlock;
@property (copy, nonatomic) UIAlertViewCompletionBlock didDismissBlock;

@property (copy, nonatomic) UIAlertViewBlock willPresentBlock;
@property (copy, nonatomic) UIAlertViewBlock didPresentBlock;
@property (copy, nonatomic) UIAlertViewBlock cancelBlock;

@property (copy, nonatomic) BOOL(^shouldEnableFirstOtherButtonBlock)(UIAlertView *alertView);

@end



#pragma mark - UIActionSheet+VictorKit

@interface UIActionSheet (VictorKit)

typedef void (^UIActionSheetBlock) (UIActionSheet *actionSheet);
typedef void (^UIActionSheetCompletionBlock) (UIActionSheet *actionSheet, NSInteger buttonIndex);

+ (instancetype)showFromTabBar:(UITabBar *)tabBar
                     withTitle:(NSString *)title
             cancelButtonTitle:(NSString *)cancelButtonTitle
        destructiveButtonTitle:(NSString *)destructiveButtonTitle
             otherButtonTitles:(NSArray *)otherButtonTitles
                      tapBlock:(UIActionSheetCompletionBlock)tapBlock;

+ (instancetype)showFromToolbar:(UIToolbar *)toolbar
                      withTitle:(NSString *)title
              cancelButtonTitle:(NSString *)cancelButtonTitle
         destructiveButtonTitle:(NSString *)destructiveButtonTitle
              otherButtonTitles:(NSArray *)otherButtonTitles
                       tapBlock:(UIActionSheetCompletionBlock)tapBlock;

+ (instancetype)showInView:(UIView *)view
                 withTitle:(NSString *)title
         cancelButtonTitle:(NSString *)cancelButtonTitle
    destructiveButtonTitle:(NSString *)destructiveButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
                  tapBlock:(UIActionSheetCompletionBlock)tapBlock;

// Tambahan Victor
+ (instancetype)showInView:(UIView *)view
                 withTitle:(NSString *)title
         cancelButtonTitle:(NSString *)cancelButtonTitle
    destructiveButtonTitle:(NSString *)destructiveButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
                  delegate:(id)delegate
                  tapBlock:(UIActionSheetCompletionBlock)tapBlock;

+ (instancetype)showFromBarButtonItem:(UIBarButtonItem *)barButtonItem
                             animated:(BOOL)animated
                            withTitle:(NSString *)title
                    cancelButtonTitle:(NSString *)cancelButtonTitle
               destructiveButtonTitle:(NSString *)destructiveButtonTitle
                    otherButtonTitles:(NSArray *)otherButtonTitles
                             tapBlock:(UIActionSheetCompletionBlock)tapBlock;

+ (instancetype)showFromRect:(CGRect)rect
                      inView:(UIView *)view
                    animated:(BOOL)animated
                   withTitle:(NSString *)title
           cancelButtonTitle:(NSString *)cancelButtonTitle
      destructiveButtonTitle:(NSString *)destructiveButtonTitle
           otherButtonTitles:(NSArray *)otherButtonTitles
                    tapBlock:(UIActionSheetCompletionBlock)tapBlock;

@property (copy, nonatomic) UIActionSheetCompletionBlock tapBlock;
@property (copy, nonatomic) UIActionSheetCompletionBlock willDismissBlock;
@property (copy, nonatomic) UIActionSheetCompletionBlock didDismissBlock;
@property (copy, nonatomic) UIActionSheetBlock willPresentBlock;
@property (copy, nonatomic) UIActionSheetBlock didPresentBlock;
@property (copy, nonatomic) UIActionSheetBlock cancelBlock;

@end



#pragma mark - NSString+VictorKit

@interface NSString (VictorKit)

- (BOOL) containsSubstring:(NSString*) string;
- (BOOL) containsSubstringInArray:(NSArray*)array;
- (NSString *) stringByDecodingHTMLEntities;
- (NSString *) stringByStrippingHTML;
- (NSString*) substringBetween:(NSString*)start and:(NSString*)end;
- (NSString *)MD5;      // Hash MD5
- (NSString *)SHA256;   // Hash SHA256

// Returns a string with the email address corrected for common domain typos.
// Supports misspelled variations for Gmail, Googlemail, Hotmail, Yahoo and Ymail.
// Replaces a ".con" suffix with ".com" for any domain.
// More domains can be easily added using a simple dictionary format.
//
// Some examples of bad email addresses that will be corrected:
//      robert@gmial.com -> robert@gmail.com        (1 error)
//      robert@gmial.con -> robert@gmail.com        (2 errors)
//      robert@hotmail.con -> robert@hotmail.com    (1 error)
//      robert@hoitmail.com -> robert@hotmail.com   (1 error)
//      robert@hoitmail.con -> robert@hotmail.com   (2 errors)
//      robert@aol.con -> robert@aol.com            (1 error)
//      robert.con@aol.con -> robert.con@aol.com    (1 error, but special case with multiple .con's)
//
// Besides correcting the typos, it also lowercases the email address.
// If the email address is invalid, returns the original value.
// Use - [isValidEmailAddress] to validate the email address first if necessary.
- (NSString *)stringByCorrectingEmailTypos;
// Validate the email syntax (not domains) using a RegEx pattern.
- (BOOL)isValidEmailAddress;

@end



#pragma mark - UIImage+VictorKit

@interface UIImage (VictorKit)

- (UIImage *)croppedImage:(CGRect)bounds;
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;

- (BOOL)hasAlpha;
- (UIImage *)imageWithAlpha;
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize;

@end


#pragma mark - NSDate+VictorKit

@interface NSDate (VictorKit)
+ (NSDate *)dateForDate:(NSDate *)sourceDateInUTC andTimeZone:(NSTimeZone*)targetTimeZone;
+ (NSDate *)currentDateForTimeZone:(NSTimeZone*)targetTimeZone;
- (NSInteger)hoursAgo;
- (NSString*)formattedAsTimeAgo;
- (NSString *)dayOfWeekString;
- (NSDate *)simpleDate; // keeps only the year, month and day of the month
+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;

+ (NSDate *) dateTomorrow;
+ (NSDate *) dateYesterday;
- (BOOL) isEqualToDateIgnoringTime: (NSDate *) aDate;
- (BOOL) isToday;
- (BOOL) isTomorrow;
- (BOOL) isYesterday;
- (BOOL) isYesterdayOrBefore;
- (BOOL) isTomorrowOrAfter;

@end


#pragma mark - UIColor+VictorKit

@interface UIColor (VictorKit)
+ (UIColor *)oppositeColorOf:(UIColor *)mainColor;
+ (UIColor *) colorWithHexString: (NSString *) hexString;
@end




#pragma mark - Code Snippet - Table View Delegate

/*
<UITableViewDelegate, UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView: (UITableView *)tableView
- (NSInteger)tableView: (UITableView *)tableView numberOfRowsInSection: (NSInteger)section
- (UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath
 {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    return cell;
}
- (CGFloat)tableView: (UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
*/

#pragma mark - Code Snippet - Collection View Delegate

/*
<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
 
- (NSInteger)collectionView: (UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section
- (UICollectionViewCell *)collectionView: (UICollectionView *)collectionView cellForItemAtIndexPath: (NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath: (NSIndexPath *)indexPath
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
*/

#pragma mark - Code Snippet - Image Picker Delegate

/*
<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
UIImagePickerController *imagePicker;
- (void) setupImagePicker
{
    self.photoPicker = [[UIImagePickerController alloc] init];
    self.photoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.photoPicker.delegate = self;
    [self presentViewController:self.photoPicker animated:TRUE completion:nil];
}
- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
- (void)imagePickerControllerDidCancel: (UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
*/

#pragma mark - Code Snippet - Singleton

/*
+ (id)shared;

+ (id)shared
{
    static TheSingleton *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}
- (id)init
{
    if (self = [super init])
    {
        // Additional init
    }
    return self;
}
- (void)dealloc
{
    // Should never be called, but just here for clarity really.
}
*/

#pragma mark - Code Snippet - Mail Compose

/*
#import <MessageUI/MessageUI.h>

- (void)presentModalMailComposerViewController:(BOOL)animated
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
        composeViewController.mailComposeDelegate = self;
        [composeViewController setSubject:<#Subject#>];
        [composeViewController setMessageBody:<#Body#> isHTML:YES];
        [composeViewController setToRecipients:@[<#Recipients#>]];
        [self presentViewController:composeViewController animated:animated completion:nil];
    } else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"<#Cannot Send Mail Message#>", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
}
// MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error)
        NSLog(@"%@", error);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
*/

#pragma mark - Code Snippet - Location Service

/*
#import <CoreLocation/CoreLocation.h>

<CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;

- (void) setupLocationManager
{
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    [self.locationManager startUpdatingLocation];
    [self.locationManager startMonitoringSignificantLocationChanges];
}

-(void) locationManager:(CLLocationManager*)manager didUpdateLocations:(NSArray *)locations
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:[locations objectAtIndex:0] completionHandler:^(NSArray *placemarks, NSError *error)
    {
        CLPlacemark *thisPlacemark = [placemarks objectAtIndex:0];
        
        NSLog(@"Placemark name : %@", thisPlacemark.name);                                      // Koorie Heritage Trust
        NSLog(@"Placemark address dictionary : %@", thisPlacemark.addressDictionary);           // Complete NSDictionary
        NSLog(@"Placemark iso country code : %@", thisPlacemark.ISOcountryCode);                // AU
        NSLog(@"Placemark country : %@", thisPlacemark.country);                                // Australia
        NSLog(@"Placemark postal code : %@", thisPlacemark.postalCode);                         // 3000
        NSLog(@"Placemark administrative area : %@", thisPlacemark.administrativeArea);         // VIC
        NSLog(@"Placemark sub adminsitrative area : %@", thisPlacemark.subAdministrativeArea);  //
        NSLog(@"Placemark locality : %@", thisPlacemark.locality);                              // Melbourne
        NSLog(@"Placemark sub locality : %@", thisPlacemark.subLocality);                       // Melbourne CBD
        NSLog(@"Placemark thoroughfare : %@", thisPlacemark.thoroughfare);                      // King Street
        NSLog(@"Placemark sub thoroughfare : %@", thisPlacemark.subThoroughfare);               // 309
        NSLog(@"Placemark region : %@", thisPlacemark.region);                                  //
    }];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location didFailWithError : %@", [error localizedDescription]);
}
*/

#pragma mark - Code Snippet - Support Orientation > iOS 6

/*
//For UIViewController's
- (BOOL)shouldAutorotate
{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}
//For Delegate
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}
*/

#pragma mark - Code Snippet - Send Text Message

/*
@import MessageUI;
<MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate>
-(void)experiment_textMessage
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = @"SMS Body";
        controller.messageComposeDelegate = self;
        controller.delegate = self;
        [self presentViewController:controller animated:TRUE completion:nil];
    } else {
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:{
            break;}
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:TRUE completion:^{
        QVLog;
    }];
}
*/

#pragma mark - Code Snippet - NSNotification

/*
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Add this instance of TestClass as an observer of the TestNotification.
    // We tell the notification center to inform us of "TestNotification"
    // notifications using the receiveTestNotification: selector. By
    // specifying object:nil, we tell the notification center that we are not
    // interested in who posted the notification. If you provided an actual
    // object rather than nil, the notification center will only notify you
    // when the notification was posted by that particular object.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    if ([[notification name] isEqualToString:@"TestNotification"])
        NSLog (@"Successfully received the test notification!");
}
- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
// Somewhere else
- (void) someMethod
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TestNotification" object:self];
}
*/

