//
//  VictorKit.m
//  VCLabs
//
//  Created by Victor Chandra on 23/05/2014.
//  Copyright (c) 2014 ThreeMonkee. All rights reserved.
//

#import "VictorKit.h"
#import <objc/runtime.h>
#import <CommonCrypto/CommonDigest.h>

#pragma mark - VictorKit Implementation

@implementation VictorKit

+ (void) phoneCallTo: (NSString*)phoneNumber
{
    NSString *finalNumber = [@"tel://" stringByAppendingString:phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalNumber]];
}

+ (void) openAppStore: (NSString*)appName
{
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.com/app/%@",appName]]];
}

+ (void) alertDebug: (NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Debug"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:FALSE];
}

+(void)alertError:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:FALSE];
}

+(void)alert:(NSString*)title
     message:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:FALSE];
}

+ (CGSize)makeSize:(CGSize)originalSize
         fitInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    
    float scale = MIN(widthScale, heightScale);
    
    CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    
    return newSize;
}

+ (CGSize)makeSize:(CGSize)originalSize
        fillInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    
    float scale = MAX(widthScale, heightScale);
    
    CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    
    return newSize;
}

+ (NSString*) appName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
}

+ (NSString*) appVersionShort
{
    // Version
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

+ (NSString*) appVersionLong
{
    // Build
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+ (NSString*) operatingSystemVersion
{
    return [[UIDevice currentDevice] systemVersion];
}

+ (NSString*) deviceModel
{
    return [[UIDevice currentDevice] model];
}

+ (NSString*) bundleIdentifier
{
    return [[NSBundle mainBundle] bundleIdentifier];
}

+ (UIImage *)radialGradientImage:(CGSize)size start:(float)start end:(float)end centre:(CGPoint)centre radius:(float)radius
{
    //Example
    //    CGSize size = self.view.bounds.size;
    //    CGPoint centre = self.view.center;
    //    float startColor = 1.0f;
    //    float endColor = 0.0f;
    //    float radius = MIN(self.view.bounds.size.width, self.view.bounds.size.height);
    //    UIImage *imageBlur = [VictorKit radialGradientImage:size start:startColor end:endColor centre:centre radius:radius];
    
    UIGraphicsBeginImageContextWithOptions(size, YES, 1);
    
    size_t count = 2;
    CGFloat locations[2] = {0.0, 1.0};
    CGFloat components[8] = {start, start, start, 1.0, end, end, end, 1.0};
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef grad = CGGradientCreateWithColorComponents (colorSpace, components, locations, count);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawRadialGradient (UIGraphicsGetCurrentContext(), grad, centre, 0, centre, radius, kCGGradientDrawsAfterEndLocation);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    CGGradientRelease(grad);
    UIGraphicsEndImageContext();
    return image;
}

+ (NSArray*) arrayOfCountryNames
{
    NSLocale *locale = [NSLocale currentLocale];
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    
    NSMutableArray *sortedCountryArray = [[NSMutableArray alloc] init];
    
    for (NSString *countryCode in countryArray)
    {
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [sortedCountryArray addObject:displayNameString];
    }
    
    [sortedCountryArray sortUsingSelector:@selector(localizedCompare:)];
    return sortedCountryArray;
}

+ (void) logAllFonts
{
    for (NSString *familyName in [UIFont familyNames])
    {
        NSLog(@"Family Name : %@", familyName);
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName])
            NSLog(@"    Font Name : %@", fontName);
    }
}

+ (void) logAllLocales
{
    for (NSString *locale in [NSLocale availableLocaleIdentifiers])
    {
        NSLog(@"Locale : %@", locale);
    }
}

+ (void) logAllUnicodes
{
    for (int i=0 ; i<65536 ; i++)
    {
//        int code = i;
//        NSString *stringCode = [NSString stringWithFormat:@"%d", code];
//        NSScanner *scanner = [NSScanner scannerWithString:stringCode];
//        [scanner scanInt:&code];
//        NSString *text = [NSString stringWithFormat:@"%C ", (unsigned short)code];
//        NSLog(@"#%d : %@", i, text);
        
        unichar x = i;
        NSString *text = [NSString stringWithCharacters:&x length:1];
        NSLog(@"#%d : %@", i, text);
    }
}

+ (NSString *)extractYoutubeID:(NSString *)youtubeURL
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)" options:NSRegularExpressionCaseInsensitive error:&error];
    //    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=clip_id=)\\d+" options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:youtubeURL options:0 range:NSMakeRange(0, [youtubeURL length])];
    if(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)))
    {
        NSString *substringForFirstMatch = [youtubeURL substringWithRange:rangeOfFirstMatch];
        return substringForFirstMatch;
    }
    return nil;
}

+ (BOOL) arrayOfString:(NSArray*)array containsString:(NSString*)string
{
    for (id thisObject in array)
    {
        if (![thisObject isKindOfClass:[NSString class]])
            continue;
        
        NSString *thisString = thisObject;
        if ([thisString isEqualToString:string])
            return TRUE;
    }
    return FALSE;
}

+ (BOOL) isEmail: (NSString*)candidate
{
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

/**
 * All items from etalonDictionary should be in dictionary. Extra keys from dictionary should be ignored
 */
+ (BOOL)isContentOfDictionary: (NSDictionary *)etalonDictionary
             partOfDictionary: (NSDictionary *)dictionary
{
    BOOL res = NO;
    for (id item in etalonDictionary){
        NSObject *secondObject = [dictionary objectForKey:item];
        if (secondObject == nil)
            return NO;
        else{
            NSObject *value = [etalonDictionary objectForKey:item];
            if ([value isKindOfClass:[NSString class]]){
                NSString *string = (NSString *) value;
                
                if ([string isEqualToString:(NSString *) secondObject]){
                    res = YES;
                } else {
                    return NO;
                }
            }
            if ([value isKindOfClass:[NSNumber class]]){
                NSNumber *number = (NSNumber *) value;
                if([number isEqualToNumber:(NSNumber *) secondObject])
                    res = YES;
                else
                    return NO;
            }
            if ([value isKindOfClass:[NSDictionary class]]){
                res = [VictorKit isContentOfDictionary:(NSDictionary *) value partOfDictionary:(NSDictionary *) secondObject];
                if (!res){
                    return NO;
                }
            }
        }
    }
    return res;
}

+ (NSArray*) uniqueArrayFromArray: (NSArray*)array
{
    NSArray *cleanedArray = [[NSSet setWithArray:array] allObjects];
    return cleanedArray;
}

+ (UIImage*) screenshotOfView: (UIView*)view
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

+ (NSDictionary*) cleanDictionary:(NSDictionary*)dictionary
{
    NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    for (NSString *key in [dictionary allKeys])
    {
        id unknownObject = [dictionary objectForKey:key];
        if ([unknownObject isKindOfClass:[NSDictionary class]])
        {
            [returnDictionary setObject:[VictorKit cleanDictionary:(NSMutableDictionary*)unknownObject]
                                 forKey:key];
        } else if ([unknownObject isKindOfClass:[NSArray class]])
        {
            [returnDictionary setObject:[VictorKit cleanArray:(NSMutableArray*)unknownObject]
                                 forKey:key];
        } else
        {
            if ((NSString*)unknownObject == (id)[NSNull null] || [unknownObject isKindOfClass:[NSNull class]])
                [returnDictionary setObject:@"" forKey:key];
        }
    }
    return returnDictionary;
}

+ (NSArray*) cleanArray:(NSArray*)array
{
    NSMutableArray *returnArray = [NSMutableArray arrayWithArray:array];
    for (int i=0 ; i<array.count ; i++)
    {
        id unknownObject = array[i];
        if ([unknownObject isKindOfClass:[NSDictionary class]])
        {
            [returnArray replaceObjectAtIndex:i
                                   withObject:[VictorKit cleanDictionary:(NSMutableDictionary*)unknownObject]];
        } else if ([unknownObject isKindOfClass:[NSArray class]])
        {
            [returnArray replaceObjectAtIndex:i
                                   withObject:[VictorKit cleanArray:(NSMutableArray*)unknownObject]];
        } else
        {
            if ((NSString*)unknownObject == (id)[NSNull null] || [unknownObject isKindOfClass:[NSNull class]])
                [returnArray replaceObjectAtIndex:i withObject:@""];
        }
    }
    return returnArray;
}

@end


#pragma mark - C Functions

CGPoint midPoint(CGRect r)
{
    return CGPointMake(CGRectGetMidX(r), CGRectGetMidY(r));
}

CATransform3D CATransform3DMakePerspective(CGFloat z) {
    CATransform3D t = CATransform3DIdentity;
    t.m34 = - 1.0 / z;
    return t;
}


#pragma mark - UIButton+VictorKit

@implementation UIButton (VictorKit)

static char overviewKey;

@dynamic actions;

- (void)setAction:(NSString *)action withBlock:(void (^)(void))block {
    
    if ([self actions] == nil) {
        [self setActions:[[NSMutableDictionary alloc] init]];
    }
    
    [[self actions] setObject:block forKey:action];
    
    if ([kUIButtonBlockTouchUpInside isEqualToString:action]) {
        [self addTarget:self action:@selector(doTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
}
- (void)setActions:(NSMutableDictionary *)actions {
    objc_setAssociatedObject(self, &overviewKey, actions, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSMutableDictionary *)actions {
    return objc_getAssociatedObject(self, &overviewKey);
}
- (void)doTouchUpInside:(id)sender {
    void(^block)(void);
    block = [[self actions] objectForKey:kUIButtonBlockTouchUpInside];
    block();
}
@end


#pragma mark - UIAlertView+VictorKit

@interface XAlertViewWrapper : NSObject
@property (copy) UIAlertViewCompletionBlock completionBlock;
@end

@implementation XAlertViewWrapper
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (self.completionBlock)
        self.completionBlock(alertView, buttonIndex);
}
- (void)alertViewCancel:(UIAlertView *)alertView{
    if (self.completionBlock)
        self.completionBlock(alertView, alertView.cancelButtonIndex);
}
@end

@implementation UIAlertView (VictorKit)

static const void *UIAlertViewOriginalDelegateKey                   = &UIAlertViewOriginalDelegateKey;
static const void *UIAlertViewTapBlockKey                           = &UIAlertViewTapBlockKey;
static const void *UIAlertViewWillPresentBlockKey                   = &UIAlertViewWillPresentBlockKey;
static const void *UIAlertViewDidPresentBlockKey                    = &UIAlertViewDidPresentBlockKey;
static const void *UIAlertViewWillDismissBlockKey                   = &UIAlertViewWillDismissBlockKey;
static const void *UIAlertViewDidDismissBlockKey                    = &UIAlertViewDidDismissBlockKey;
static const void *UIAlertViewCancelBlockKey                        = &UIAlertViewCancelBlockKey;
static const void *UIAlertViewShouldEnableFirstOtherButtonBlockKey  = &UIAlertViewShouldEnableFirstOtherButtonBlockKey;

+ (instancetype)showWithTitle:(NSString *)title
                      message:(NSString *)message
                        style:(UIAlertViewStyle)style
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSArray *)otherButtonTitles
                     tapBlock:(UIAlertViewCompletionBlock)tapBlock
{
    NSString *firstObject = otherButtonTitles.count ? otherButtonTitles[0] : nil;
    
    UIAlertView *alertView = [[self alloc] initWithTitle:title
                                                 message:message
                                                delegate:nil
                                       cancelButtonTitle:cancelButtonTitle
                                       otherButtonTitles:firstObject, nil];
    
    alertView.alertViewStyle = style;
    
    if (otherButtonTitles.count > 1) {
        for (NSString *buttonTitle in [otherButtonTitles subarrayWithRange:NSMakeRange(1, otherButtonTitles.count - 1)]) {
            [alertView addButtonWithTitle:buttonTitle];
        }
    }
    
    if (tapBlock) {
        alertView.tapBlock = tapBlock;
    }
    
    [alertView show];
    
#if !__has_feature(objc_arc)
    return [alertView autorelease];
#else
    return alertView;
#endif
}

+ (instancetype)showWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSArray *)otherButtonTitles
                     tapBlock:(UIAlertViewCompletionBlock)tapBlock {
    
    return [self showWithTitle:title
                       message:message
                         style:UIAlertViewStyleDefault
             cancelButtonTitle:cancelButtonTitle
             otherButtonTitles:otherButtonTitles
                      tapBlock:tapBlock];
}

- (void)_checkAlertViewDelegate {
    if (self.delegate != (id<UIAlertViewDelegate>)self) {
        objc_setAssociatedObject(self, UIAlertViewOriginalDelegateKey, self.delegate, OBJC_ASSOCIATION_ASSIGN);
        self.delegate = (id<UIAlertViewDelegate>)self;
    }
}

- (UIAlertViewCompletionBlock)tapBlock {
    return objc_getAssociatedObject(self, UIAlertViewTapBlockKey);
}

- (void)setTapBlock:(UIAlertViewCompletionBlock)tapBlock {
    [self _checkAlertViewDelegate];
    objc_setAssociatedObject(self, UIAlertViewTapBlockKey, tapBlock, OBJC_ASSOCIATION_COPY);
}

- (UIAlertViewCompletionBlock)willDismissBlock {
    return objc_getAssociatedObject(self, UIAlertViewWillDismissBlockKey);
}

- (void)setWillDismissBlock:(UIAlertViewCompletionBlock)willDismissBlock {
    [self _checkAlertViewDelegate];
    objc_setAssociatedObject(self, UIAlertViewWillDismissBlockKey, willDismissBlock, OBJC_ASSOCIATION_COPY);
}

- (UIAlertViewCompletionBlock)didDismissBlock {
    return objc_getAssociatedObject(self, UIAlertViewDidDismissBlockKey);
}

- (void)setDidDismissBlock:(UIAlertViewCompletionBlock)didDismissBlock {
    [self _checkAlertViewDelegate];
    objc_setAssociatedObject(self, UIAlertViewDidDismissBlockKey, didDismissBlock, OBJC_ASSOCIATION_COPY);
}

- (UIAlertViewBlock)willPresentBlock {
    return objc_getAssociatedObject(self, UIAlertViewWillPresentBlockKey);
}

- (void)setWillPresentBlock:(UIAlertViewBlock)willPresentBlock {
    [self _checkAlertViewDelegate];
    objc_setAssociatedObject(self, UIAlertViewWillPresentBlockKey, willPresentBlock, OBJC_ASSOCIATION_COPY);
}

- (UIAlertViewBlock)didPresentBlock {
    return objc_getAssociatedObject(self, UIAlertViewDidPresentBlockKey);
}

- (void)setDidPresentBlock:(UIAlertViewBlock)didPresentBlock {
    [self _checkAlertViewDelegate];
    objc_setAssociatedObject(self, UIAlertViewDidPresentBlockKey, didPresentBlock, OBJC_ASSOCIATION_COPY);
}

- (UIAlertViewBlock)cancelBlock {
    return objc_getAssociatedObject(self, UIAlertViewCancelBlockKey);
}

- (void)setCancelBlock:(UIAlertViewBlock)cancelBlock {
    [self _checkAlertViewDelegate];
    objc_setAssociatedObject(self, UIAlertViewCancelBlockKey, cancelBlock, OBJC_ASSOCIATION_COPY);
}

- (void)setShouldEnableFirstOtherButtonBlock:(BOOL(^)(UIAlertView *alertView))shouldEnableFirstOtherButtonBlock {
    [self _checkAlertViewDelegate];
    objc_setAssociatedObject(self, UIAlertViewShouldEnableFirstOtherButtonBlockKey, shouldEnableFirstOtherButtonBlock, OBJC_ASSOCIATION_COPY);
}

- (BOOL(^)(UIAlertView *alertView))shouldEnableFirstOtherButtonBlock {
    return objc_getAssociatedObject(self, UIAlertViewShouldEnableFirstOtherButtonBlockKey);
}

// UIAlertViewDelegate
- (void)willPresentAlertView:(UIAlertView *)alertView {
    UIAlertViewBlock block = alertView.willPresentBlock;
    
    if (block) {
        block(alertView);
    }
    
    id originalDelegate = objc_getAssociatedObject(self, UIAlertViewOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(willPresentAlertView:)]) {
        [originalDelegate willPresentAlertView:alertView];
    }
}

- (void)didPresentAlertView:(UIAlertView *)alertView {
    UIAlertViewBlock block = alertView.didPresentBlock;
    
    if (block) {
        block(alertView);
    }
    
    id originalDelegate = objc_getAssociatedObject(self, UIAlertViewOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(didPresentAlertView:)]) {
        [originalDelegate didPresentAlertView:alertView];
    }
}


- (void)alertViewCancel:(UIAlertView *)alertView {
    UIAlertViewBlock block = alertView.cancelBlock;
    
    if (block) {
        block(alertView);
    }
    
    id originalDelegate = objc_getAssociatedObject(self, UIAlertViewOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(alertViewCancel:)]) {
        [originalDelegate alertViewCancel:alertView];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIAlertViewCompletionBlock completion = alertView.tapBlock;
    
    if (completion) {
        completion(alertView, buttonIndex);
    }
    
    id originalDelegate = objc_getAssociatedObject(self, UIAlertViewOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)]) {
        [originalDelegate alertView:alertView clickedButtonAtIndex:buttonIndex];
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    UIAlertViewCompletionBlock completion = alertView.willDismissBlock;
    
    if (completion) {
        completion(alertView, buttonIndex);
    }
    
    id originalDelegate = objc_getAssociatedObject(self, UIAlertViewOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(alertView:willDismissWithButtonIndex:)]) {
        [originalDelegate alertView:alertView willDismissWithButtonIndex:buttonIndex];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    UIAlertViewCompletionBlock completion = alertView.didDismissBlock;
    
    if (completion) {
        completion(alertView, buttonIndex);
    }
    
    id originalDelegate = objc_getAssociatedObject(self, UIAlertViewOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(alertView:didDismissWithButtonIndex:)]) {
        [originalDelegate alertView:alertView didDismissWithButtonIndex:buttonIndex];
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    BOOL(^shouldEnableFirstOtherButtonBlock)(UIAlertView *alertView) = alertView.shouldEnableFirstOtherButtonBlock;
    
    if (shouldEnableFirstOtherButtonBlock) {
        return shouldEnableFirstOtherButtonBlock(alertView);
    }
    
    id originalDelegate = objc_getAssociatedObject(self, UIAlertViewOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(alertViewShouldEnableFirstOtherButton:)]) {
        return [originalDelegate alertViewShouldEnableFirstOtherButton:alertView];
    }
    
    return YES;
}

static const char kXAlertViewWrapper;

- (void)showWithCompletion:(UIAlertViewCompletionBlock)completionBlock
{
    XAlertViewWrapper *alertViewWrapper = XAlertViewWrapper.new;
    
    alertViewWrapper.completionBlock = completionBlock;
    self.delegate = alertViewWrapper;
    
    objc_setAssociatedObject(self, &kXAlertViewWrapper, alertViewWrapper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self show];
}

@end



#pragma mark - UIActionSheet+VictorKit

@implementation UIActionSheet (Blocks)

static const void *UIActionSheetOriginalDelegateKey = &UIActionSheetOriginalDelegateKey;
static const void *UIActionSheetTapBlockKey = &UIActionSheetTapBlockKey;
static const void *UIActionSheetWillPresentBlockKey = &UIActionSheetWillPresentBlockKey;
static const void *UIActionSheetDidPresentBlockKey = &UIActionSheetDidPresentBlockKey;
static const void *UIActionSheetWillDismissBlockKey = &UIActionSheetWillDismissBlockKey;
static const void *UIActionSheetDidDismissBlockKey = &UIActionSheetDidDismissBlockKey;
static const void *UIActionSheetCancelBlockKey = &UIActionSheetCancelBlockKey;
#define NSArrayObjectMaybeNil(__ARRAY__, __INDEX__) ((__INDEX__ >= [__ARRAY__ count]) ? nil : [__ARRAY__ objectAtIndex:__INDEX__])
// This is a hack to turn an array into a variable argument list. There is no good way to expand arrays into variable argument lists in Objective-C. This works by nil-terminating the list as soon as we overstep the bounds of the array. The obvious glitch is that we only support a finite number of buttons.
#define NSArrayToVariableArgumentsList(__ARRAYNAME__) NSArrayObjectMaybeNil(__ARRAYNAME__, 0), NSArrayObjectMaybeNil(__ARRAYNAME__, 1), NSArrayObjectMaybeNil(__ARRAYNAME__, 2), NSArrayObjectMaybeNil(__ARRAYNAME__, 3), NSArrayObjectMaybeNil(__ARRAYNAME__, 4), NSArrayObjectMaybeNil(__ARRAYNAME__, 5), NSArrayObjectMaybeNil(__ARRAYNAME__, 6), NSArrayObjectMaybeNil(__ARRAYNAME__, 7), NSArrayObjectMaybeNil(__ARRAYNAME__, 8), NSArrayObjectMaybeNil(__ARRAYNAME__, 9), nil

+ (instancetype)showFromTabBar:(UITabBar *)tabBar
                     withTitle:(NSString *)title
             cancelButtonTitle:(NSString *)cancelButtonTitle
        destructiveButtonTitle:(NSString *)destructiveButtonTitle
             otherButtonTitles:(NSArray *)otherButtonTitles
                      tapBlock:(UIActionSheetCompletionBlock)tapBlock {
    UIActionSheet *actionSheet = [[self alloc] initWithTitle:title
                                                    delegate:nil
                                           cancelButtonTitle:cancelButtonTitle
                                      destructiveButtonTitle:destructiveButtonTitle
                                           otherButtonTitles:NSArrayToVariableArgumentsList(otherButtonTitles)];
    if (tapBlock) {
        actionSheet.tapBlock = tapBlock;
    }
    [actionSheet showFromTabBar:tabBar];
#if !__has_feature(objc_arc)
    return [actionSheet autorelease];
#else
    return actionSheet;
#endif
}

+ (instancetype)showFromToolbar:(UIToolbar *)toolbar
                      withTitle:(NSString *)title
              cancelButtonTitle:(NSString *)cancelButtonTitle
         destructiveButtonTitle:(NSString *)destructiveButtonTitle
              otherButtonTitles:(NSArray *)otherButtonTitles
                       tapBlock:(UIActionSheetCompletionBlock)tapBlock {
    UIActionSheet *actionSheet = [[self alloc] initWithTitle:title
                                                    delegate:nil
                                           cancelButtonTitle:cancelButtonTitle
                                      destructiveButtonTitle:destructiveButtonTitle
                                           otherButtonTitles:NSArrayToVariableArgumentsList(otherButtonTitles)];
    if (tapBlock) {
        actionSheet.tapBlock = tapBlock;
    }
    [actionSheet showFromToolbar:toolbar];
#if !__has_feature(objc_arc)
    return [actionSheet autorelease];
#else
    return actionSheet;
#endif
}

+ (instancetype)showInView:(UIView *)view
                 withTitle:(NSString *)title
         cancelButtonTitle:(NSString *)cancelButtonTitle
    destructiveButtonTitle:(NSString *)destructiveButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
                  tapBlock:(UIActionSheetCompletionBlock)tapBlock {
    UIActionSheet *actionSheet = [[self alloc] initWithTitle:title
                                                    delegate:nil
                                           cancelButtonTitle:cancelButtonTitle
                                      destructiveButtonTitle:destructiveButtonTitle
                                           otherButtonTitles:NSArrayToVariableArgumentsList(otherButtonTitles)];
    if (tapBlock) {
        actionSheet.tapBlock = tapBlock;
    }
    [actionSheet showInView:view];
#if !__has_feature(objc_arc)
    return [actionSheet autorelease];
#else
    return actionSheet;
#endif
}

// Tambahan Victor
+ (instancetype)showInView:(UIView *)view
                 withTitle:(NSString *)title
         cancelButtonTitle:(NSString *)cancelButtonTitle
    destructiveButtonTitle:(NSString *)destructiveButtonTitle
         otherButtonTitles:(NSArray *)otherButtonTitles
                  delegate:(id)delegate
                  tapBlock:(UIActionSheetCompletionBlock)tapBlock
{
    UIActionSheet *actionSheet = [[self alloc] initWithTitle:title
                                                    delegate:delegate
                                           cancelButtonTitle:cancelButtonTitle
                                      destructiveButtonTitle:destructiveButtonTitle
                                           otherButtonTitles:NSArrayToVariableArgumentsList(otherButtonTitles)];
    if (tapBlock) {
        actionSheet.tapBlock = tapBlock;
    }
    [actionSheet showInView:view];
#if !__has_feature(objc_arc)
    return [actionSheet autorelease];
#else
    return actionSheet;
#endif
}

+ (instancetype)showFromBarButtonItem:(UIBarButtonItem *)barButtonItem
                             animated:(BOOL)animated
                            withTitle:(NSString *)title
                    cancelButtonTitle:(NSString *)cancelButtonTitle
               destructiveButtonTitle:(NSString *)destructiveButtonTitle
                    otherButtonTitles:(NSArray *)otherButtonTitles
                             tapBlock:(UIActionSheetCompletionBlock)tapBlock {
    UIActionSheet *actionSheet = [[self alloc] initWithTitle:title
                                                    delegate:nil
                                           cancelButtonTitle:cancelButtonTitle
                                      destructiveButtonTitle:destructiveButtonTitle
                                           otherButtonTitles:NSArrayToVariableArgumentsList(otherButtonTitles)];
    if (tapBlock) {
        actionSheet.tapBlock = tapBlock;
    }
    [actionSheet showFromBarButtonItem:barButtonItem animated:animated];
#if !__has_feature(objc_arc)
    return [actionSheet autorelease];
#else
    return actionSheet;
#endif
}

+ (instancetype)showFromRect:(CGRect)rect
                      inView:(UIView *)view
                    animated:(BOOL)animated
                   withTitle:(NSString *)title
           cancelButtonTitle:(NSString *)cancelButtonTitle
      destructiveButtonTitle:(NSString *)destructiveButtonTitle
           otherButtonTitles:(NSArray *)otherButtonTitles
                    tapBlock:(UIActionSheetCompletionBlock)tapBlock {
    UIActionSheet *actionSheet = [[self alloc] initWithTitle:title
                                                    delegate:nil
                                           cancelButtonTitle:cancelButtonTitle
                                      destructiveButtonTitle:destructiveButtonTitle
                                           otherButtonTitles:NSArrayToVariableArgumentsList(otherButtonTitles)];
    if (tapBlock) {
        actionSheet.tapBlock = tapBlock;
    }
    [actionSheet showFromRect:rect inView:view animated:animated];
#if !__has_feature(objc_arc)
    return [actionSheet autorelease];
#else
    return actionSheet;
#endif
}

- (void)_checkActionSheetDelegate {
    if (self.delegate != (id<UIActionSheetDelegate>)self) {
        objc_setAssociatedObject(self, UIActionSheetOriginalDelegateKey, self.delegate, OBJC_ASSOCIATION_ASSIGN);
        self.delegate = (id<UIActionSheetDelegate>)self;
    }
}
- (UIActionSheetCompletionBlock)tapBlock {
    return objc_getAssociatedObject(self, UIActionSheetTapBlockKey);
}
- (void)setTapBlock:(UIActionSheetCompletionBlock)tapBlock {
    [self _checkActionSheetDelegate];
    objc_setAssociatedObject(self, UIActionSheetTapBlockKey, tapBlock, OBJC_ASSOCIATION_COPY);
}
- (UIActionSheetCompletionBlock)willDismissBlock {
    return objc_getAssociatedObject(self, UIActionSheetWillDismissBlockKey);
}
- (void)setWillDismissBlock:(UIActionSheetCompletionBlock)willDismissBlock {
    [self _checkActionSheetDelegate];
    objc_setAssociatedObject(self, UIActionSheetWillDismissBlockKey, willDismissBlock, OBJC_ASSOCIATION_COPY);
}
- (UIActionSheetCompletionBlock)didDismissBlock {
    return objc_getAssociatedObject(self, UIActionSheetDidDismissBlockKey);
}
- (void)setDidDismissBlock:(UIActionSheetCompletionBlock)didDismissBlock {
    [self _checkActionSheetDelegate];
    objc_setAssociatedObject(self, UIActionSheetDidDismissBlockKey, didDismissBlock, OBJC_ASSOCIATION_COPY);
}
- (UIActionSheetBlock)willPresentBlock {
    return objc_getAssociatedObject(self, UIActionSheetWillPresentBlockKey);
}
- (void)setWillPresentBlock:(UIActionSheetBlock)willPresentBlock {
    [self _checkActionSheetDelegate];
    objc_setAssociatedObject(self, UIActionSheetWillPresentBlockKey, willPresentBlock, OBJC_ASSOCIATION_COPY);
}
- (UIActionSheetBlock)didPresentBlock {
    return objc_getAssociatedObject(self, UIActionSheetDidPresentBlockKey);
}
- (void)setDidPresentBlock:(UIActionSheetBlock)didPresentBlock {
    [self _checkActionSheetDelegate];
    objc_setAssociatedObject(self, UIActionSheetDidPresentBlockKey, didPresentBlock, OBJC_ASSOCIATION_COPY);
}
- (UIActionSheetBlock)cancelBlock {
    return objc_getAssociatedObject(self, UIActionSheetCancelBlockKey);
}
- (void)setCancelBlock:(UIActionSheetBlock)cancelBlock {
    [self _checkActionSheetDelegate];
    objc_setAssociatedObject(self, UIActionSheetCancelBlockKey, cancelBlock, OBJC_ASSOCIATION_COPY);
}

// UIActionSheetDelegate
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    UIActionSheetBlock completion = actionSheet.willPresentBlock;
    if (completion) {
        completion(actionSheet);
    }
    id originalDelegate = objc_getAssociatedObject(self, UIActionSheetOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(willPresentActionSheet:)]) {
        [originalDelegate willPresentActionSheet:actionSheet];
    }
}
- (void)didPresentActionSheet:(UIActionSheet *)actionSheet {
    UIActionSheetBlock completion = actionSheet.didPresentBlock;
    if (completion) {
        completion(actionSheet);
    }
    id originalDelegate = objc_getAssociatedObject(self, UIActionSheetOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(didPresentActionSheet:)]) {
        [originalDelegate didPresentActionSheet:actionSheet];
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIActionSheetCompletionBlock completion = actionSheet.tapBlock;
    if (completion) {
        completion(actionSheet, buttonIndex);
    }
    id originalDelegate = objc_getAssociatedObject(self, UIActionSheetOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(actionSheet:clickedButtonAtIndex:)]) {
        [originalDelegate actionSheet:actionSheet clickedButtonAtIndex:buttonIndex];
    }
}
- (void)actionSheetCancel:(UIActionSheet *)actionSheet {
    UIActionSheetBlock completion = actionSheet.cancelBlock;
    if (completion) {
        completion(actionSheet);
    }
    id originalDelegate = objc_getAssociatedObject(self, UIActionSheetOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(actionSheetCancel:)]) {
        [originalDelegate actionSheetCancel:actionSheet];
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    UIActionSheetCompletionBlock completion = actionSheet.willDismissBlock;
    if (completion) {
        completion(actionSheet, buttonIndex);
    }
    id originalDelegate = objc_getAssociatedObject(self, UIActionSheetOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(actionSheet:willDismissWithButtonIndex:)]) {
        [originalDelegate actionSheet:actionSheet willDismissWithButtonIndex:buttonIndex];
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    UIActionSheetCompletionBlock completion = actionSheet.didDismissBlock;
    if (completion) {
        completion(actionSheet, buttonIndex);
    }
    id originalDelegate = objc_getAssociatedObject(self, UIActionSheetOriginalDelegateKey);
    if (originalDelegate && [originalDelegate respondsToSelector:@selector(actionSheet:didDismissWithButtonIndex:)]) {
        [originalDelegate actionSheet:actionSheet didDismissWithButtonIndex:buttonIndex];
    }
}
@end


#pragma mark - NSString+VictorKit

@implementation NSString (VictorKit)

typedef unsigned char * (* hashing_algorithm_t)(const void *data, CC_LONG len, unsigned char *hash);

- (BOOL) containsSubstring : (NSString*) string
{
    if ([self rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
        return FALSE;
    else
        return TRUE;
}

- (BOOL) containsSubstringInArray:(NSArray*)array
{
    for (NSString *string in array)
        if ([self rangeOfString:string options:NSCaseInsensitiveSearch].location != NSNotFound)
            return TRUE;
    
    return FALSE;
}

- (NSString *)stringByDecodingHTMLEntities
{
    NSUInteger myLength = [self length];
    NSUInteger ampIndex = [self rangeOfString:@"&" options:NSLiteralSearch].location;
    
    if (ampIndex == NSNotFound) return self;
    
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25f)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:self];
    [scanner setCharactersToBeSkipped:nil];
    
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
    
    do {
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        
        if ([scanner isAtEnd]) goto finish;
        
        if ([scanner scanString:@"&amp;" intoString:NULL]) {
            [result appendString:@"&"];
        } else if ([scanner scanString:@"&apos;" intoString:NULL]) {
            [result appendString:@"'"];
        } else if ([scanner scanString:@"&quot;" intoString:NULL]) {
            [result appendString:@"\""];
        } else if ([scanner scanString:@"&lt;" intoString:NULL]) {
            [result appendString:@"<"];
        } else if ([scanner scanString:@"&gt;" intoString:NULL]) {
            [result appendString:@">"];
        } else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            
            if (gotNumber) {
                [result appendFormat:@"%C", (unichar)charCode];
                [scanner scanString:@";" intoString:NULL];
            } else {
                NSString *unknownEntity = nil;
                [scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
                [result appendFormat:@"&#%@%@", xForHex, unknownEntity];
            }
        } else {
            NSString *amp;
            
            [scanner scanString:@"&" intoString:&amp];      //an isolated & symbol
            [result appendString:amp];
        }
    } while (![scanner isAtEnd]);
    
finish:
    return result;
}

- (NSString *) stringByStrippingHTML
{
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (NSString*) substringBetween: (NSString*)start and: (NSString*)end
{
    NSRange r1 = [self rangeOfString:start];
    NSRange r2 = [self rangeOfString:end];
    NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
    
    if (rSub.location == NSNotFound)
        return nil;
    else
        return [self substringWithRange:rSub];
}

- (NSString *)hashAs:(hashing_algorithm_t)algorithm withSize:(size_t)size
{
    if (!self.length)
        return nil;
    
    char const *bytes = self.UTF8String;
    unsigned char hash[size];
    
    algorithm(bytes, (CC_LONG)strlen(bytes), hash);
    
    NSMutableString *ret = [NSMutableString.alloc initWithCapacity:2 * size];
    
    for (NSInteger i = 0; i < size; ++i)
        [ret appendFormat:@"%02x", hash[i]];
    
    return ret;
}

- (NSString *)MD5
{
    return [self hashAs:CC_MD5 withSize:CC_MD5_DIGEST_LENGTH];
}

- (NSString *)SHA256
{
    return [self hashAs:CC_SHA256 withSize:CC_SHA256_DIGEST_LENGTH];
}

// These are the correct versions of the domain names.
static const NSString *gmailDomain        = @"gmail.com";
static const NSString *googleMailDomain   = @"googlemail.com";
static const NSString *hotmailDomain      = @"hotmail.com";
static const NSString *yahooDomain        = @"yahoo.com";
static const NSString *yahooMailDomain    = @"ymail.com";

- (NSString *)stringByCorrectingEmailTypos
{
    if (![self isValidEmailAddress]) {
        NSLog(@"%@ is not a valid email address.", self);
        return self;
    }
    
    // Start with a lower-cased version of the original string.
    __block NSString *correctedEmailAddress = [self lowercaseString];
    
    // First replace a ".con" suffix with ".com".
    if ([correctedEmailAddress hasSuffix:@".con"]) {
        NSRange range = NSMakeRange(correctedEmailAddress.length - 4, 4);
        correctedEmailAddress = [correctedEmailAddress stringByReplacingOccurrencesOfString:@".con"
                                                                                 withString:@".com"
                                                                                    options:NSBackwardsSearch|NSAnchoredSearch
                                                                                      range:range];
    }
    
    // Now iterate through the bad domain names to find common typos.
    // Feel free to add to the dictionary below.
    // I got the original list from http://getintheinbox.com/2013/02/25/typo-traps/
    NSDictionary *typos = @{@"gogglemail.com":  googleMailDomain,
                            @"googlmail.com":   googleMailDomain,
                            @"goglemail.com":   googleMailDomain,
                            @"hotmial.com":     hotmailDomain,
                            @"hotmal.com":      hotmailDomain,
                            @"hoitmail.com":    hotmailDomain,
                            @"homail.com":      hotmailDomain,
                            @"hotnail.com":     hotmailDomain,
                            @"hotrmail.com":    hotmailDomain,
                            @"hotmil.com":      hotmailDomain,
                            @"hotmaill.com":    hotmailDomain,
                            @"yaho.com":        yahooDomain,
                            @"uahoo.com":       yahooDomain,
                            @"ayhoo.com":       yahooDomain,
                            @"ymial.com":       yahooMailDomain,
                            @"ymaill.com":      yahooMailDomain,
                            @"gmal.com":        gmailDomain,
                            @"gnail.com":       gmailDomain,
                            @"gmaill.com":      gmailDomain,
                            @"gmial.com":       gmailDomain,
                            };
    
    [typos enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        NSString *domainIncludingAtSymbol = [NSString stringWithFormat:@"@%@", key];
        if ([correctedEmailAddress hasSuffix:domainIncludingAtSymbol]) {
            // Found a bad domain.
            correctedEmailAddress = [correctedEmailAddress stringByReplacingOccurrencesOfString:key withString:object];
            *stop = YES;
        }
    }];
    
    return correctedEmailAddress;
}

- (BOOL)isValidEmailAddress
{
    // http://stackoverflow.com/questions/3139619/check-that-an-email-address-is-valid-on-ios
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

@end



#pragma mark - UIImage+VictorKit

@implementation UIImage (VictorKit)

// Returns a copy of this image that is cropped to the given bounds.
// The bounds will be adjusted using CGRectIntegral.
// This method ignores the image's imageOrientation setting.
- (UIImage *)croppedImage:(CGRect)bounds
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], bounds);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

// Returns a copy of this image that is squared to the thumbnail size.
// If transparentBorder is non-zero, a transparent border of the given size will be added around the edges of the thumbnail. (Adding a transparent border of at least one pixel in size has the side-effect of antialiasing the edges of the image when rotating it using Core Animation.)
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality {
    UIImage *resizedImage = [self resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                       bounds:CGSizeMake(thumbnailSize, thumbnailSize)
                                         interpolationQuality:quality];
    
    // Crop out any part of the image that's larger than the thumbnail size
    // The cropped rect must be centered on the resized image
    // Round the origin points so that the size isn't altered when CGRectIntegral is later invoked
    CGRect cropRect = CGRectMake(round((resizedImage.size.width - thumbnailSize) / 2),
                                 round((resizedImage.size.height - thumbnailSize) / 2),
                                 thumbnailSize,
                                 thumbnailSize);
    UIImage *croppedImage = [resizedImage croppedImage:cropRect];
    
    UIImage *transparentBorderImage = borderSize ? [croppedImage transparentBorderImage:borderSize] : croppedImage;
    
    return [transparentBorderImage roundedCornerImage:cornerRadius borderSize:borderSize];
}

// Returns a rescaled copy of the image, taking into account its orientation
// The image will be scaled disproportionately if necessary to fit the bounds specified by the parameter
- (UIImage *)resizedImage:(CGSize)newSize interpolationQuality:(CGInterpolationQuality)quality {
    BOOL drawTransposed;
    
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            drawTransposed = YES;
            break;
            
        default:
            drawTransposed = NO;
    }
    
    return [self resizedImage:newSize
                    transform:[self transformForOrientation:newSize]
               drawTransposed:drawTransposed
         interpolationQuality:quality];
}

// Resizes the image according to the given content mode, taking into account the image's orientation
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality {
    CGFloat horizontalRatio = bounds.width / self.size.width;
    CGFloat verticalRatio = bounds.height / self.size.height;
    CGFloat ratio;
    
    switch (contentMode) {
        case UIViewContentModeScaleAspectFill:
            ratio = MAX(horizontalRatio, verticalRatio);
            break;
            
        case UIViewContentModeScaleAspectFit:
            ratio = MIN(horizontalRatio, verticalRatio);
            break;
            
        default:
            [NSException raise:NSInvalidArgumentException format:@"Unsupported content mode: %d", (int)contentMode];
    }
    
    CGSize newSize = CGSizeMake(self.size.width * ratio, self.size.height * ratio);
    
    return [self resizedImage:newSize interpolationQuality:quality];
}

// Private helper methods

// Returns a copy of the image that has been transformed using the given affine transform and scaled to the new size
// The new image's orientation will be UIImageOrientationUp, regardless of the current image's orientation
// If the new size is not integral, it will be rounded up
- (UIImage *)resizedImage:(CGSize)newSize
                transform:(CGAffineTransform)transform
           drawTransposed:(BOOL)transpose
     interpolationQuality:(CGInterpolationQuality)quality
{
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGRect transposedRect = CGRectMake(0, 0, newRect.size.height, newRect.size.width);
    CGImageRef imageRef = self.CGImage;
    
    // Build a context that's the same dimensions as the new size
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newRect.size.width,
                                                newRect.size.height,
                                                CGImageGetBitsPerComponent(imageRef),
                                                0,
                                                CGImageGetColorSpace(imageRef),
                                                CGImageGetBitmapInfo(imageRef));
    
    // Rotate and/or flip the image if required by its orientation
    CGContextConcatCTM(bitmap, transform);
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(bitmap, quality);
    
    // Draw into the context; this scales the image
    CGContextDrawImage(bitmap, transpose ? transposedRect : newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    // Clean up
    CGContextRelease(bitmap);
    CGImageRelease(newImageRef);
    
    return newImage;
}

// Returns an affine transform that takes into account the image orientation when drawing a scaled image
- (CGAffineTransform)transformForOrientation:(CGSize)newSize
{
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:           // EXIF = 3
        case UIImageOrientationDownMirrored:   // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, newSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:           // EXIF = 6
        case UIImageOrientationLeftMirrored:   // EXIF = 5
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:          // EXIF = 8
        case UIImageOrientationRightMirrored:  // EXIF = 7
            transform = CGAffineTransformTranslate(transform, 0, newSize.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
            
        default:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:     // EXIF = 2
        case UIImageOrientationDownMirrored:   // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:   // EXIF = 5
        case UIImageOrientationRightMirrored:  // EXIF = 7
            transform = CGAffineTransformTranslate(transform, newSize.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        default:
            break;
    }
    
    return transform;
}

// Returns true if the image has an alpha layer
- (BOOL)hasAlpha {
    CGImageAlphaInfo alpha = CGImageGetAlphaInfo(self.CGImage);
    return (alpha == kCGImageAlphaFirst ||
            alpha == kCGImageAlphaLast ||
            alpha == kCGImageAlphaPremultipliedFirst ||
            alpha == kCGImageAlphaPremultipliedLast);
}

// Returns a copy of the given image, adding an alpha channel if it doesn't already have one
- (UIImage *)imageWithAlpha {
    if ([self hasAlpha]) {
        return self;
    }
    
    CGImageRef imageRef = self.CGImage;
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    
    // The bitsPerComponent and bitmapInfo values are hard-coded to prevent an "unsupported parameter combination" error
    CGContextRef offscreenContext = CGBitmapContextCreate(NULL,
                                                          width,
                                                          height,
                                                          8,
                                                          0,
                                                          CGImageGetColorSpace(imageRef),
                                                          kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedFirst);
    
    // Draw the image into the context and retrieve the new image, which will now have an alpha layer
    CGContextDrawImage(offscreenContext, CGRectMake(0, 0, width, height), imageRef);
    CGImageRef imageRefWithAlpha = CGBitmapContextCreateImage(offscreenContext);
    UIImage *imageWithAlpha = [UIImage imageWithCGImage:imageRefWithAlpha];
    
    // Clean up
    CGContextRelease(offscreenContext);
    CGImageRelease(imageRefWithAlpha);
    
    return imageWithAlpha;
}

// Returns a copy of the image with a transparent border of the given size added around its edges.
// If the image has no alpha layer, one will be added to it.
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize {
    // If the image does not have an alpha layer, add one
    UIImage *image = [self imageWithAlpha];
    
    CGRect newRect = CGRectMake(0, 0, image.size.width + borderSize * 2, image.size.height + borderSize * 2);
    
    // Build a context that's the same dimensions as the new size
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newRect.size.width,
                                                newRect.size.height,
                                                CGImageGetBitsPerComponent(self.CGImage),
                                                0,
                                                CGImageGetColorSpace(self.CGImage),
                                                CGImageGetBitmapInfo(self.CGImage));
    
    // Draw the image in the center of the context, leaving a gap around the edges
    CGRect imageLocation = CGRectMake(borderSize, borderSize, image.size.width, image.size.height);
    CGContextDrawImage(bitmap, imageLocation, self.CGImage);
    CGImageRef borderImageRef = CGBitmapContextCreateImage(bitmap);
    
    // Create a mask to make the border transparent, and combine it with the image
    CGImageRef maskImageRef = [self newBorderMask:borderSize size:newRect.size];
    CGImageRef transparentBorderImageRef = CGImageCreateWithMask(borderImageRef, maskImageRef);
    UIImage *transparentBorderImage = [UIImage imageWithCGImage:transparentBorderImageRef];
    
    // Clean up
    CGContextRelease(bitmap);
    CGImageRelease(borderImageRef);
    CGImageRelease(maskImageRef);
    CGImageRelease(transparentBorderImageRef);
    
    return transparentBorderImage;
}

// Private helper methods

// Creates a mask that makes the outer edges transparent and everything else opaque
// The size must include the entire mask (opaque part + transparent border)
// The caller is responsible for releasing the returned reference by calling CGImageRelease
- (CGImageRef)newBorderMask:(NSUInteger)borderSize size:(CGSize)size {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Build a context that's the same dimensions as the new size
    CGContextRef maskContext = CGBitmapContextCreate(NULL,
                                                     size.width,
                                                     size.height,
                                                     8, // 8-bit grayscale
                                                     0,
                                                     colorSpace,
                                                     kCGBitmapByteOrderDefault | kCGImageAlphaNone);
    
    // Start with a mask that's entirely transparent
    CGContextSetFillColorWithColor(maskContext, [UIColor blackColor].CGColor);
    CGContextFillRect(maskContext, CGRectMake(0, 0, size.width, size.height));
    
    // Make the inner part (within the border) opaque
    CGContextSetFillColorWithColor(maskContext, [UIColor whiteColor].CGColor);
    CGContextFillRect(maskContext, CGRectMake(borderSize, borderSize, size.width - borderSize * 2, size.height - borderSize * 2));
    
    // Get an image of the context
    CGImageRef maskImageRef = CGBitmapContextCreateImage(maskContext);
    
    // Clean up
    CGContextRelease(maskContext);
    CGColorSpaceRelease(colorSpace);
    
    return maskImageRef;
}


// Creates a copy of this image with rounded corners
// If borderSize is non-zero, a transparent border of the given size will also be added
// Original author: Björn Sållarp. Used with permission. See: http://blog.sallarp.com/iphone-uiimage-round-corners/
- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize {
    // If the image does not have an alpha layer, add one
    UIImage *image = [self imageWithAlpha];
    
    // Build a context that's the same dimensions as the new size
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 image.size.width,
                                                 image.size.height,
                                                 CGImageGetBitsPerComponent(image.CGImage),
                                                 0,
                                                 CGImageGetColorSpace(image.CGImage),
                                                 CGImageGetBitmapInfo(image.CGImage));
    
    // Create a clipping path with rounded corners
    CGContextBeginPath(context);
    [self addRoundedRectToPath:CGRectMake(borderSize, borderSize, image.size.width - borderSize * 2, image.size.height - borderSize * 2)
                       context:context
                     ovalWidth:cornerSize
                    ovalHeight:cornerSize];
    CGContextClosePath(context);
    CGContextClip(context);
    
    // Draw the image to the context; the clipping path will make anything outside the rounded rect transparent
    CGContextDrawImage(context, CGRectMake(0, 0, image.size.width, image.size.height), image.CGImage);
    
    // Create a CGImage from the context
    CGImageRef clippedImage = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    // Create a UIImage from the CGImage
    UIImage *roundedImage = [UIImage imageWithCGImage:clippedImage];
    CGImageRelease(clippedImage);
    
    return roundedImage;
}


// Private helper methods

// Adds a rectangular path to the given context and rounds its corners by the given extents
// Original author: Björn Sållarp. Used with permission. See: http://blog.sallarp.com/iphone-uiimage-round-corners/
- (void)addRoundedRectToPath:(CGRect)rect context:(CGContextRef)context ovalWidth:(CGFloat)ovalWidth ovalHeight:(CGFloat)ovalHeight
{
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM(context, ovalWidth, ovalHeight);
    CGFloat fw = CGRectGetWidth(rect) / ovalWidth;
    CGFloat fh = CGRectGetHeight(rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

@end


#pragma mark - NSDate+VictorKit

@implementation NSDate (VictorKit)
#define SECOND 1
#define MINUTE (SECOND * 60)
#define HOUR (MINUTE * 60)
#define DAY (HOUR * 24)
#define WEEK (DAY * 7)
#define MONTH (DAY * 31)
#define YEAR (DAY * 365.24)
+ (NSDate *)currentDateForTimeZone:(NSTimeZone *)targetTimeZone
{
    return [[self class] dateForDate:[NSDate date] andTimeZone:targetTimeZone];
}
+ (NSDate *)dateForDate:(NSDate *)sourceDateInUTC andTimeZone:(NSTimeZone *)targetTimeZone
{
    NSDate *result= nil;
    
    //===
    
    if (targetTimeZone)
    {
        NSCalendarUnit unitFlags =
        (NSCalendarUnitMinute |
         NSCalendarUnitHour |
         NSCalendarUnitDay |
         NSCalendarUnitMonth |
         NSCalendarUnitYear |
         NSCalendarUnitTimeZone);
        
        //===
        
        NSCalendar *targetCalendar = [NSCalendar currentCalendar];
        targetCalendar.timeZone = targetTimeZone;
        
        //===
        
        NSDateComponents *targetComponents =
        [targetCalendar
         components:unitFlags
         fromDate:sourceDateInUTC]; // given dateAndTime in UTC
        
        // lets re-set time zone to avoid
        // back date and time conversion when we
        // will convert components into NSDate instance
        
        targetComponents.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        
        //===
        
        result = [targetCalendar dateFromComponents:targetComponents];
    }
    
    //===
    
    return result;
}
- (NSInteger) hoursAgo
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour) fromDate:self toDate:[NSDate date] options:0];
    return [components hour];
}
- (NSString *)formattedAsTimeAgo
{
    //Now
    NSDate *now = [NSDate date];
    NSTimeInterval secondsSince = -(int)[self timeIntervalSinceDate:now];
    //Should never hit this but handle the future case
    if(secondsSince < 0)
        return @"In The Future";
    // < 1 minute = "Just now"
    if(secondsSince < MINUTE)
        return @"Just now";
    // < 1 hour = "x minutes ago"
    if(secondsSince < HOUR)
        return [self formatMinutesAgo:secondsSince];
    // Today = "x hours ago"
    if([self isSameDayAs:now])
        return [self formatAsToday:secondsSince];
    // Yesterday = "Yesterday at 1:28 PM"
    if([self isYesterday:now])
        return [self formatAsYesterday];
    // < Last 7 days = "Friday at 1:48 AM"
    if([self isLastWeek:secondsSince])
        return [self formatAsLastWeek];
    // < Last 30 days = "March 30 at 1:14 PM"
    if([self isLastMonth:secondsSince])
        return [self formatAsLastMonth];
    // < 1 year = "September 15"
    if([self isLastYear:secondsSince])
        return [self formatAsLastYear];
    // Anything else = "September 9, 2011"
    return [self formatAsOther];
}
/*
 ========================== Date Comparison Methods ==========================
 */
/*
 Is Same Day As
 Checks to see if the dates are the same calendar day
 */
- (BOOL)isSameDayAs:(NSDate *)comparisonDate
{
    //Check by matching the date strings
    NSDateFormatter *dateComparisonFormatter = [[NSDateFormatter alloc] init];
    [dateComparisonFormatter setDateFormat:@"yyyy-MM-dd"];
    //Return true if they are the same
    return [[dateComparisonFormatter stringFromDate:self] isEqualToString:[dateComparisonFormatter stringFromDate:comparisonDate]];
}
/*
 If the current date is yesterday relative to now
 Pasing in now to be more accurate (time shift during execution) in the calculations
 */
- (BOOL)isYesterday:(NSDate *)now
{
    return [self isSameDayAs:[now dateBySubtractingDays:1]];
}
//From https://github.com/erica/NSDate-Extensions/blob/master/NSDate-Utilities.m
- (NSDate *) dateBySubtractingDays: (NSInteger) numDays
{
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + DAY * -numDays;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}
/*
 Is Last Week
 We want to know if the current date object is the first occurance of
 that day of the week (ie like the first friday before today
 - where we would colloquially say "last Friday")
 ( within 6 of the last days)
 TODO: make this more precise (1 week ago, if it is 7 days ago check the exact date)
 */
- (BOOL)isLastWeek:(NSTimeInterval)secondsSince
{
    return secondsSince < WEEK;
}
/*
 Is Last Month
 Previous 31 days?
 TODO: Validate on fb
 TODO: Make last day precise
 */
- (BOOL)isLastMonth:(NSTimeInterval)secondsSince
{
    return secondsSince < MONTH;
}
/*
 Is Last Year
 TODO: Make last day precise
 */
- (BOOL)isLastYear:(NSTimeInterval)secondsSince
{
    return secondsSince < YEAR;
}
/*
 =============================================================================
 */
/*
 ========================== Formatting Methods ==========================
 */
// < 1 hour = "x minutes ago"
- (NSString *)formatMinutesAgo:(NSTimeInterval)secondsSince
{
    //Convert to minutes
    int minutesSince = (int)secondsSince / MINUTE;
    //Handle Plural
    if(minutesSince == 1)
        return @"1 minute ago";
    else
        return [NSString stringWithFormat:@"%d minutes ago", minutesSince];
}
// Today = "x hours ago"
- (NSString *)formatAsToday:(NSTimeInterval)secondsSince
{
    //Convert to hours
    int hoursSince = (int)secondsSince / HOUR;
    //Handle Plural
    if(hoursSince == 1)
        return @"1 hour ago";
    else
        return [NSString stringWithFormat:@"%d hours ago", hoursSince];
}
// Yesterday = "Yesterday at 1:28 PM"
- (NSString *)formatAsYesterday
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //Format
    [dateFormatter setDateFormat:@"h:mm a"];
    return [NSString stringWithFormat:@"Yesterday at %@", [dateFormatter stringFromDate:self]];
}
// < Last 7 days = "Friday at 1:48 AM"
- (NSString *)formatAsLastWeek
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //Format
    [dateFormatter setDateFormat:@"EEEE 'at' h:mm a"];
    return [dateFormatter stringFromDate:self];
}
// < Last 30 days = "March 30 at 1:14 PM"
- (NSString *)formatAsLastMonth
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //Format
    [dateFormatter setDateFormat:@"MMMM d 'at' h:mm a"];
    return [dateFormatter stringFromDate:self];
}
// < 1 year = "September 15"
- (NSString *)formatAsLastYear
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //Format
    [dateFormatter setDateFormat:@"MMMM d"];
    return [dateFormatter stringFromDate:self];
}
// Anything else = "September 9, 2011"
- (NSString *)formatAsOther
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //Format
    [dateFormatter setDateFormat:@"LLLL d, yyyy"];
    return [dateFormatter stringFromDate:self];
}
- (NSString *)dayOfWeekString
{
    NSDateFormatter *dayOfWeekOnlyFormatter = [[NSDateFormatter alloc] init];
    [dayOfWeekOnlyFormatter setDateFormat:@"EEEE"];
    return [dayOfWeekOnlyFormatter stringFromDate:self];
}
// keeps only the year, month and day of the month
- (NSDate *)simpleDate
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self];
    return [cal dateFromComponents:components];
}
#define DATE_COMPONENTS (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal)

#define CURRENT_CALENDAR [NSCalendar currentCalendar]

#define D_MINUTE	60
#define D_HOUR		3600
#define D_DAY		86400
#define D_WEEK		604800
#define D_YEAR		31556926

+ (NSDate *) dateWithDaysFromNow: (NSUInteger) days
{
    NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] + D_DAY * days;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}
+ (NSDate *) dateWithDaysBeforeNow: (NSUInteger) days
{
    NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] - D_DAY * days;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}
+ (NSDate *) dateTomorrow
{
    return [NSDate dateWithDaysFromNow:1];
}
+ (NSDate *) dateYesterday
{
    return [NSDate dateWithDaysBeforeNow:1];
}
- (BOOL) isEqualToDateIgnoringTime: (NSDate *) aDate
{
    NSDateComponents *components1 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
    NSDateComponents *components2 = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:aDate];
    return (([components1 year] == [components2 year]) &&
            ([components1 month] == [components2 month]) &&
            ([components1 day] == [components2 day]));
}
- (BOOL) isToday
{
    return [self isEqualToDateIgnoringTime:[NSDate date]];
}
- (BOOL) isTomorrow
{
    return [self isEqualToDateIgnoringTime:[NSDate dateTomorrow]];
}
- (BOOL) isYesterday
{
    return [self isEqualToDateIgnoringTime:[NSDate dateYesterday]];
}
- (BOOL) isYesterdayOrBefore
{
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* componentsNow = [calendar components:flags fromDate:[NSDate date]];
    NSDateComponents* componentsSelf = [calendar components:flags fromDate:self];
    NSDate* dateOnlyNow = [calendar dateFromComponents:componentsNow];
    NSDate* dateOnlySelf = [calendar dateFromComponents:componentsSelf];
    return ([dateOnlySelf earlierDate:dateOnlyNow] == dateOnlySelf);
}
- (BOOL) isTomorrowOrAfter
{
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* componentsNow = [calendar components:flags fromDate:[NSDate date]];
    NSDateComponents* componentsSelf = [calendar components:flags fromDate:self];
    NSDate* dateOnlyNow = [calendar dateFromComponents:componentsNow];
    NSDate* dateOnlySelf = [calendar dateFromComponents:componentsSelf];
    return ([dateOnlySelf laterDate:dateOnlyNow] == dateOnlySelf);
}
+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}
@end


#pragma mark - UIColor+VictorKit

@implementation UIColor (VictorKit)
+ (UIColor *)oppositeColorOf:(UIColor *)mainColor
{
    /*
     + (UIColor *)blackColor;      // 0.0 white
     + (UIColor *)darkGrayColor;   // 0.333 white
     + (UIColor *)lightGrayColor;  // 0.667 white
     + (UIColor *)whiteColor;      // 1.0 white
     + (UIColor *)grayColor;       // 0.5 white
     */
    if ([mainColor isEqual:[UIColor blackColor]]) {
        mainColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    }
    else if ([mainColor isEqual:[UIColor darkGrayColor]]) {
        mainColor = [UIColor colorWithRed:84.915/255.f green:84.915/255.f blue:84.915/255.f alpha:1];
    }
    else if ([mainColor isEqual:[UIColor lightGrayColor]]) {
        mainColor = [UIColor colorWithRed:170.085/255.f green:170.085/255.f blue:170.085/255.f alpha:1];
    }
    else if ([mainColor isEqual:[UIColor whiteColor]]) {
        mainColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    }
    else if ([mainColor isEqual:[UIColor grayColor]]) {
        mainColor = [UIColor colorWithRed:127.5/255.f green:127.5/255.f blue:127.5/255.f alpha:1];
    }
    
    const CGFloat *componentColors = CGColorGetComponents(mainColor.CGColor);
    UIColor *convertedColor = [[UIColor alloc] initWithRed:(1.0 - componentColors[0])
                                                     green:(1.0 - componentColors[1])
                                                      blue:(1.0 - componentColors[2])
                                                     alpha:componentColors[3]];
    return convertedColor;
}
+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
+ (UIColor *) colorWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            return nil;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
@end
