//
//  VCLabsTests.swift
//  VCLabsTests
//
//  Created by Victor Chandra on 22/12/2015.
//  Copyright © 2015 Victor. All rights reserved.
//

import XCTest
@testable import VCLabs

class VCLabsTests: XCTestCase
{
    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testLocale() {
        let availableLocaleIdentifiers = NSLocale.availableLocaleIdentifiers
        print(availableLocaleIdentifiers)
        
        let isoCountryCodes = NSLocale.isoCountryCodes
        print(isoCountryCodes)
        
        let isoLanguageCodes = NSLocale.isoLanguageCodes
        print(isoLanguageCodes)
        
        let isoCurrencyCodes = NSLocale.isoCurrencyCodes
        print(isoCurrencyCodes)
        
        let commonISOCurrencyCodes = NSLocale.commonISOCurrencyCodes
        print(commonISOCurrencyCodes)
        
        let preferredLanguages = NSLocale.preferredLanguages
        print(preferredLanguages)
    }
}
