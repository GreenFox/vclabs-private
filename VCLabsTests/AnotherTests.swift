//
//  AnotherTests.swift
//  VCLabs
//
//  Created by Victor Chandra on 20/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import XCTest
@testable import VCLabs

class AnotherTests: XCTestCase
{
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // Foundation Framework test
    func testStringPart()
    {
        let stringSmall = "nation"
        let stringLarge = "international"
        XCTAssert(stringLarge.contains(stringSmall), "Test failed")
    }
    
    func testRandom()
    {
        let limit = 100
        let max = 10
        for index in 1...limit
        {
            let random = SwiftSingleton.getRandom(max)
            print("Try #\(index)", random)
            XCTAssert(random < max, "More than max")
        }
    }
}
