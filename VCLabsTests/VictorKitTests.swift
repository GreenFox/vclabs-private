//
//  VictorKitTests.swift
//  VCLabs
//
//  Created by Victor Chandra on 20/01/2016.
//  Copyright © 2016 Victor. All rights reserved.
//

import XCTest
@testable import VCLabs

class VictorKitTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIsEmail()
    {
        let arrayEmails = ["example@gmail.com","example@yahoo.com","example@hotmail.com","ex.am.ple@gmail.com","ex4mpl3@gmail.com","123@gmail.com"]
        let arrayNotEmails = ["notemail","12345","","-_-","...","+-*/","@#$%","example@mail","example.com"]

        for candidate in arrayEmails
        {
            XCTAssert(VictorKit.isEmail(candidate), "Test failed")
        }
        for candidate in arrayNotEmails
        {
            XCTAssertFalse(VictorKit.isEmail(candidate), "Test failed")
        }
    }
    
    func testArrayContainsString ()
    {
        let arrayStrings = ["one","two","three","four"]
        let rightString = "two"
        let wrongString = "seven"
        XCTAssert(VictorKit.array(ofString: arrayStrings, contains: rightString), "Test failed")
        XCTAssertFalse(VictorKit.array(ofString: arrayStrings, contains: wrongString), "Test failed")
    }
    
    func testDictContainsDict ()
    {
        let largeDictionary = ["key_1": "value_1", "key_2": "value_2", "key_3": "value_3"]
        let rightDictionary = ["key_3": "value_3"]
        let wrongDictionary = ["food": "burger"]
        XCTAssert(VictorKit.isContent(of: rightDictionary, partOf: largeDictionary), "Test failed")
        XCTAssertFalse(VictorKit.isContent(of: wrongDictionary, partOf: largeDictionary), "Test failed")
    }
    
    func testScreenshotOfView()
    {
        let view = UIView(frame: CGRect(x: 0,y: 0,width: 100,height: 100))
        XCTAssertNotNil(VictorKit.screenshot(of: view), "View empty")
    }
    
    func testUniqueArrayFromArray()
    {
        let array_1 = ["one", "two", "three", "four", "one", "two"]
        let array_2 = ["one", "two", "three", "four"]
        XCTAssertNotNil(VictorKit.uniqueArray(from: array_1), "Unique array is nil")
        XCTAssertEqual(VictorKit.uniqueArray(from: array_1).count, array_2.count, "Unique array is not equal")
    }
    
    func testRadialGradientImage ()
    {
        let image = VictorKit.radialGradientImage(CGSize(width: 100,height: 100), start: 0, end: 100, centre: CGPoint(x: 50,y: 50), radius: 50)
        XCTAssertNotNil(image, "Radial image is nil")
    }
    
    func testDeviceInformations()
    {
        XCTAssertNotNil(VictorKit.appName(), "Info nil")
        XCTAssertNotNil(VictorKit.appVersionShort(), "Info nil")
        XCTAssertNotNil(VictorKit.appVersionLong(), "Info nil")
        XCTAssertNotNil(VictorKit.operatingSystemVersion(), "Info nil")
        XCTAssertNotNil(VictorKit.deviceModel(), "Info nil")
        XCTAssertNotNil(VictorKit.bundleIdentifier(), "Info nil")
    }

    func testArrayCountryNames()
    {
        XCTAssertNotNil(VictorKit.arrayOfCountryNames(), "Country names nil")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
